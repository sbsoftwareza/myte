export const environment = {
  production: false,
  apiUrl: 'https://cartrack.cavalryapps.com/dashboard-assistant-api/bot-api/public/alpha',

  dashboardConfig : {

      menu : {

        pages : [
            //example pages
          {
            pageName : "View Conversations",
            pageDescription : "View all Conversations",
            linkText : "Conversations",
            linkUrl: "dashboard/model-list/botUser",
            icon : "claims"
          }

        ]

      },

      modelSettings : {

         botUser : {

          partialInfoSetup : {
            name : "botUser", //Name of the model to pull data from
            queryExtras : [
              {
                key : 'user_role' , 
                value : "Car-Track-Bot-User"
              }, 
              // {
              //   key : 'mid' , 
              //   value : "285"
              // }
            ],
            modelTitle: "Conversation",
            contextRelationShip: "oneToOne", //oneToOne , oneToMany
            button : {
              action : "resorce-link", //model-view or resorce-link
              resorce_link : "/view_PDF"
            },
            status : "formFieldsEntities->IBM->QueryStatus", //if model has no status flags available, leave empty
            fields: [
                {
                  key : "Conversation Id",
                  value : "id"
                },
                {
                  key : "Dialog Turn Count",
                  value : "formFieldsEntities->IBM->dialog_turn_counter" //example path in the json model to grab the value from. Starts with eith system object or form fields object
                },
                {
                  key : "Query Status",
                  value : "formFieldsEntities->IBM->QueryStatus"
                },
                {
                  key : "User Intent",
                  value : "formFieldsEntities->IBM->dialog_topics"
                },
                {
                  key : "Date Created",
                  value : "created"
                },
            ]  
          },

          accodionDisplayInfo : {
            title : "User Details",
            flagKey : "",
            flagValue : "",
            sections: [
              {title : "Conversation Details", section : "IBM", objectType: "formFieldsEntities"}
            ]
          }

         },

         userContextModels : {
          models : [
          ]
        }
      },

      landingPageConfig : {

        actions : [
        ],

        recentModels : {
          //array of recent items to be displayed on the page, each model should list the keys and value to be displayed
          models: [
            'botUser'
          ],
        },

        dataTable : {
          models : ['botUser']
        },

        components: {
            actions : false,
            recentModels : true,
            modelDataTable: false,
            graphLineBar: false,
            graphChartBar: false         
        }

      },

      analytics : {

        botAnalytics : {
           
        }
      }

    
  }
};
