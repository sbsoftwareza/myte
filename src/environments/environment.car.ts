// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://localhost/BOT_API_V2/bot-api/public/alpha',

  dashboardConfig : {

      menu : {

        pages : [

          {
            pageName : "View Conversations",
            pageDescription : "View all Conversations",
            linkText : "Conversations",
            linkUrl: "dashboard/model-list/botUser",
            icon : "claims"
          }

        ]

      },

      modelSettings : {

         botUser : {

          partialInfoSetup : {
            name : "botUser", //Name of the model to pull data from
            queryExtras : [
              {
                key : 'user_role' , 
                value : "Car-Track-Bot-User"
              }, 
              {
                key : 'mid' , 
                value : "24"
              }
            ],
            modelTitle: "Conversation",
            contextRelationShip: "oneToOne", //oneToOne , oneToMany
            status : "formFieldsEntities->IBM->conversation_id", //if model has no status flags available, leave empty
            fields: [
                {
                  key : "Conversation Id",
                  value : "formFieldsEntities->IBM->conversation_id"
                },
                {
                  key : "Dialog Turn Count",
                  value : "formFieldsEntities->IBM->dialog_turn_counter" //example path in the json model to grab the value from. Starts with eith system object or form fields object
                },
                {
                  key : "Query Status",
                  value : "formFieldsEntities->IBM->query_status"
                },
                {
                  key : "User Intent",
                  value : "formFieldsEntities->IBM->user_intent"
                },
                {
                  key : "Date Created",
                  value : "formFieldsEntities->IBM->date"
                },
            ]  
          },

          accodionDisplayInfo : {
            title : "User Details",
            flagKey : "",
            flagValue : "",
            sections: [
              {title : "Conversation Details", section : "IBM", objectType: "formFieldsEntities"}
            ]
          }

         },

         userContextModels : {
          models : [
            // {
            //   name : "Product", //Name of the model to pull data from
            //   modelTitle: "Products",
            //   searchKey : "context_id" //can be any search query params allowed in get query ie: context_id, mid, mids, formFieldsEntities->sectionName->sectionKey
            // },

            // {
            //   name : "Order", //Name of the model to pull data from
            //   modelTitle: "Orders",
            //   searchKey : "context_id" //can be any search query params allowed in get query ie: context_id, mid, mids, formFieldsEntities->sectionName->sectionKey
            // },
          ]
        }
      },

      landingPageConfig : {

        actions : [
          {
            name : "Conversations report",
            icon : "my_policy.svg",
            description : "View the daily, weekly, monthly or yearly conversations report. ",
            linkText : "View report",
            linkUrl : "/dashboard/reports/Order",
            modal : true,
            modalName : "email" //email, download, form
          },

          {
            name : "Conversations report",
            icon : "profile.svg",
            description : "View the daily, weekly, monthly or yearly conversations report. ",
            linkText : "View report",
            linkUrl : "/dashboard/reports/user/seller",
            modal : false
          },

          {
            name : "Top selling products",
            icon : "my_policy.svg",
            description : "View the daily, weekly, monthly or yearly conversations report.",
            linkText : "View report",
            linkUrl : "/dashboard/model-details",
            modal : false
          },
        ],

        recentModels : {
          //array of recent items to be displayed on the page, each model should list the keys and value to be displayed
          models: [
            'botUser'
          ],
        },

        dataTable : {
          models : ['botUser']
        },

        components: {
            actions : false,
            recentModels : false,
            modelDataTable: true,
            graphLineBar: false,
            graphChartBar: false         
        }

      }

    
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
