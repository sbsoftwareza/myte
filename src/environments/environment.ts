// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // apiUrl: 'http://localhost/Projects/CaptisInvestmentApp/CaptisAPI/bot-api/public/alpha',
  // apiUrl: 'http://3.123.117.58/alpha',
  // filesSrc: 'http://3.123.117.58',
  // apiUrl: 'http://localhost/Projects/CaptisInvestmentApp/CaptisAPI/bot-api/public/alpha',
  // filesSrc: 'http://localhost/Projects/CaptisInvestmentApp/CaptisAPI/bot-api/public',
  // apiUrl: 'http://localhost/Projects/Captis/captis-investment/bot-api/public/alpha',
  // filesSrc: 'http://localhost/Projects/Captis/captis-investment/bot-api/public',
  apiUrl: 'http://localhost/Projects/CaptisInvestmentApp/Alpha/bot-api/public/alpha',
  filesSrc: 'http://localhost/Projects/CaptisInvestmentApp/Alpha/bot-api/public',

  dashboardConfig : {

      menu : {

        pages : [
            //example pages
          {
            pageName : "E-Commerce",
            pageDescription : "E-Commerce Dashboard",
            linkText : "E-Commerce",
            linkUrl: "dashboard/ecommerce-modules/dashboard",
            // icon : "claims.svg"
          },

          {
            pageName : "Courier",
            pageDescription : "Courier Dashboard",
            linkText : "Courier",
            linkUrl: "dashboard/courier-modules/dashboard",
            // icon : "claims.svg"
          },

          {
            pageName : "Accounts",
            pageDescription : "Accounts Dashboard",
            linkText : "Accounts",
            linkUrl: "dashboard/accounts/dashboard"
          },

          {
            pageName : "Account",
            pageDescription : "Account",
            linkText : "Account",
            linkUrl: "dashboard/ecommerce-modules/seller/1"
          }

        ]

      },

      modelSettings : {

        Invoice : {

          partialInfoSetup : {
            name : "Invoice", //Name of the model to pull data from
            queryExtras : [
              // {
              //   key : 'user_role' , 
              //   value : "Car-Track-Bot-User"
              // }, 
              // {
              //   key : 'mid' , 
              //   value : "285"
              // }
            ],
            modelTitle: "Invoice",
            contextRelationShip: "oneToOne", //oneToOne , oneToMany
            button : {
              action : "resorce-link", //model-view or resorce-link
              resorce_link : "/view_PDF"
              //   action : "dshboard-link", //model-view or resorce-link or dshboard-link
               //   resorce_link : "/dashboard/ecommerce-modules/seller/"
            },
            status : "formFieldsEntities->companyDetails->name", //if model has no status flags available, leave empty
            fields: [
                {
                  key : "Date Created",
                  value : "created"
                },
                {
                  key : "Company",
                  value : "formFieldsEntities->companyDetails->name" //example path in the json model to grab the value from. Starts with eith system object or form fields object
                },
                {
                  key : "Amount",
                  value : "formFieldsEntities->invoiceDetails->total"
                },
                {
                  key : "Status",
                  value : "formFieldsEntities->invoiceDetails->status"
                },
                {
                  key : "Reference",
                  value : "formFieldsEntities->companyDetails->email"
                },
            ]  
          },

          createModel : true,
          createModelSettings : {
            sections : [ 
              {key : "invoiceDetails", label : "Invoice Details", required : true},
              {key : "companyDetails", label : "Company Details", required : false}
            ]
          },

          accodionDisplayInfo : {
            title : "User Details",
            flagKey : "",
            flagValue : "",
            sections: [
              {title : "Conversation Details", section : "invoiceDetails", objectType: "formFieldsEntities"}
            ]
          }

         },

         Product: {

          partialInfoSetup : {
            name : "Product", //Name of the model to pull data from
            queryExtras : [
              // {
              //   key : 'user_role' , 
              //   value : "Car-Track-Bot-User"
              // }, 
              // {
              //   key : 'mid' , 
              //   value : "285"
              // }
            ],
            modelTitle: "Product",
            contextRelationShip: "oneToMany", //oneToOne , oneToMany
            // button : {
            //   action : "resorce-link", //model-view or resorce-link
            //   resorce_link : "/view_PDF"
            // },
            status : "formFieldsEntities->productSettings->status", //if model has no status flags available, leave empty
            fields: [
                {
                  key : "Date Created",
                  value : "created"
                },
                {
                  key : "Name",
                  value : "formFieldsEntities->productDetails->name" //example path in the json model to grab the value from. Starts with eith system object or form fields object
                },
                {
                  key : "Price",
                  value : "formFieldsEntities->productDetails->regular_price"
                },
                {
                  key : "Department",
                  value : "formFieldsEntities->productDetails->department"
                },
                {
                  key : "Category",
                  value : "formFieldsEntities->productDetails->categories"
                },
            ]  
          },

          createModel : false,
          createModelSettings : {
            sections : [ 
              {key : "productDetails", label : "Product Details", required : true}            ]
          },

          files: {
            flagKey : "Product Images",
          },

          accodionDisplayInfo : {
            title : "Product Details",
            flagKey : "",
            flagValue : "",
            sections: [
              {title : "Product Details", section : "productDetails", objectType: "formFieldsEntities", excludes: "sizevariations "},
              {title : "Shipping Details", section : "productShipping", objectType: "formFieldsEntities", excludes: "shipping_required, warehouse_required, selectedCouriers, acceptShippmentTermsAndConditions,requiresTransportationPermit "}
            ]
          }

         },

         BackOrder: {

          partialInfoSetup : {
            name : "BackOrder", //Name of the model to pull data from
            queryExtras : [],
            modelTitle: "BackOrder",
            contextRelationShip: "oneToMany", //oneToOne , oneToMany
            status : "systemManagedEntities->orderDetails->status", //if model has no status flags available, leave empty
            fields: [
                {
                  key : "Date Created",
                  value : "created"
                },
                {
                  key : "Date Packed",
                  value : "systemManagedEntities->orderDetails->packedOnDate" //example path in the json model to grab the value from. Starts with eith system object or form fields object
                },
                {
                  key : "Reference Number",
                  value : "systemManagedEntities->orderDetails->refNumber"
                },
                {
                  key : "Due Date",
                  value : "systemManagedEntities->orderDetails->dueDate"
                },
                {
                  key : "Status",
                  value : "systemManagedEntities->orderDetails->status"
                },
            ]  
          },

          createModel : false,
          createModelSettings : {
            sections : [ 
              {key : "orderDetails", label : "Order Details", required : true}            ]
          },

          // files: {
          //   flagKey : "Product Images",
          // },

          accodionDisplayInfo : {
            title : "Order Details",
            flagKey : "",
            flagValue : "",
            sections: [
              {title : "Order Details", section : "orderDetails", objectType: "systemManagedEntities", excludes: "orderId, items "},
            ]
          },

          customFunctions: {
            edit: false,
            delete: false
          },

          dynamicFunctions: [
            {
              action: "update-status",
              title: "Update Backorder Status",
              options: [
                // {label: "Inventory Received", val: "INVENTORY_READY"},
                {label: "Order Packed", val: "PACKAGED"},
                {label: "Quality Checked", val: "QUALITY_CHECK"},
                {label: "Ready For Collection", val: "READYFORCOLLECTION"},
                {label: "Dispatched For Delivery", val: "DISPATCHED"},
              ],
              key : "systemManagedEntities->orderDetails->status",
              endpoint: "update-backorder-status"
            }
          ],

          //only set to true for custom order details view
          viewBackOrderItems : true

         },

         INBOUNDWAYBILL: {

          partialInfoSetup : {
            name : "INBOUNDWAYBILL", //Name of the model to pull data from
            queryExtras : [],
            modelTitle: "Inbound Waybill",
            contextRelationShip: "oneToMany", //oneToOne , oneToMany
            status : "systemManagedEntities->waybillDetails->status", //if model has no status flags available, leave empty
            fields: [
                {
                  key : "Date Created",
                  value : "created"
                },
                {
                  key : "Order Reference",
                  value : "systemManagedEntities->waybillDetails->refNumber" //example path in the json model to grab the value from. Starts with eith system object or form fields object
                },
                
                {
                  key : "Due Date",
                  value : "systemManagedEntities->waybillDetails->expectedDate"
                },
                {
                  key : "Collection Pin",
                  value : "systemManagedEntities->waybillDetails->collectionPin"
                },

                {
                  key : "Status",
                  value : "systemManagedEntities->waybillDetails->status"
                },
            ]  
          },

          createModel : false,
          createModelSettings : {
            sections : [ 
              {key : "waybillDetails", label : "Waybill Details", required : true}            ]
          },

          // files: {
          //   flagKey : "Product Images",
          // },

          accodionDisplayInfo : {
            title : "Waybill Details",
            flagKey : "",
            flagValue : "",
            sections: [
              {title : "Waybill Details", section : "waybillDetails", objectType: "systemManagedEntities", excludes: "orderId "},
            ]
          },

          viewInboundWaybillItems: true,

          customFunctions: {
            edit: false,
            delete: false
          },

          dynamicFunctions : [
            {
              action: "update-status",
              title: "Mark As Received",
              options: [
                {label: "Parcel Received", val: "RECEIVED"},
                {label: "Parcel Rejected", val: "REJECTED"},
                {label: "Parcel Items Incomplete", val: "INCOMPLETE_ITEMS"},
              ],
              key : "systemManagedEntities->waybillDetails->status",
              endpoint : "receive-inbound-parcel"
            }
          ]

         },

         OUTBOUNDWAYBILL: {

          partialInfoSetup : {
            name : "OUTBOUNDWAYBILL", //Name of the model to pull data from
            queryExtras : [],
            modelTitle: "Inbound Waybill",
            contextRelationShip: "oneToMany", //oneToOne , oneToMany
            status : "systemManagedEntities->waybillDetails->status", //if model has no status flags available, leave empty
            fields: [
                {
                  key : "Date Created",
                  value : "created"
                },
                {
                  key : "Order Reference",
                  value : "systemManagedEntities->waybillDetails->refNumber" //example path in the json model to grab the value from. Starts with eith system object or form fields object
                },
                
                {
                  key : "Due Date",
                  value : "systemManagedEntities->waybillDetails->expectedDate"
                },
                {
                  key : "Collection Pin",
                  value : "systemManagedEntities->waybillDetails->collectionPin"
                },

                {
                  key : "Status",
                  value : "systemManagedEntities->waybillDetails->status"
                },
            ]  
          },

          createModel : false,
          createModelSettings : {
            sections : [ 
              {key : "waybillDetails", label : "Waybill Details", required : true}            ]
          },

          // files: {
          //   flagKey : "Product Images",
          // },

          accodionDisplayInfo : {
            title : "Waybill Details",
            flagKey : "",
            flagValue : "",
            sections: [
              {title : "Waybill Details", section : "waybillDetails", objectType: "systemManagedEntities", excludes: "orderId "},
            ]
          },

          viewInboundWaybillItems: true,

          customFunctions: {
            edit: false,
            delete: false
          },

          dynamicFunctions : [
            {
              action: "update-status",
              title: "Update Waybill Status",
              options: [
                {label: "Parcel Delivered", val: "DISPACTHED"},
                {label: "Parcel Returned", val: "RETURNED"},
              ],
              key : "systemManagedEntities->waybillDetails->status",
              // endpoint : "receive-inbound-parcel"
            }
          ]

         },

         Departments: {

          partialInfoSetup : {
            name : "Departments", //Name of the model to pull data from
            queryExtras : [],
            modelTitle: "Department",
            contextRelationShip: "oneToMany", //oneToOne , oneToMany
            status : "formFieldsEntities->departmentDetails->category_status", //if model has no status flags available, leave empty
            fields: [
                {
                  key : "Date Created",
                  value : "created"
                },
                {
                  key : "Name",
                  value : "formFieldsEntities->departmentDetails->name" //example path in the json model to grab the value from. Starts with eith system object or form fields object
                },
                {
                  key : "Description",
                  value : "formFieldsEntities->departmentDetails->description"
                },
                {
                  key : "Date Updated",
                  value : "updated"
                },
                {
                  key : "Status",
                  value : "formFieldsEntities->departmentDetails->category_status"
                },
            ]  
          },

          createModel : true,
          createModelSettings : {
            sections : [ 
              {key : "departmentDetails", label : "Department Details", required : true}            ]
          },

          files: {
            flagKey : "Department Images",
          },

          accodionDisplayInfo : {
            title : "Department Details",
            flagKey : "",
            flagValue : "",
            sections: [
              {title : "Department Details", section : "departmentDetails", objectType: "formFieldsEntities", excludes: "department_images "},
            ]
          }

         },

         ShopCategory: {

          partialInfoSetup : {
            name : "ShopCategory", //Name of the model to pull data from
            queryExtras : [],
            modelTitle: "Shop Category",
            contextRelationShip: "oneToMany", //oneToOne , oneToMany
            status : "formFieldsEntities->categoryDetails->category_status", //if model has no status flags available, leave empty
            fields: [
                {
                  key : "Date Created",
                  value : "created"
                },
                {
                  key : "Name",
                  value : "formFieldsEntities->categoryDetails->name" //example path in the json model to grab the value from. Starts with eith system object or form fields object
                },
                {
                  key : "Description",
                  value : "formFieldsEntities->categoryDetails->description"
                },
                {
                  key : "Date Updated",
                  value : "updated"
                },
                {
                  key : "Status",
                  value : "formFieldsEntities->categoryDetails->category_status"
                },
            ]  
          },

          createModel : true,
          createModelSettings : {
            sections : [ 
              {key : "categoryDetails", label : "Category Details", required : true}            ]
          },

          files: {
            flagKey : "Department Images",
          },

          accodionDisplayInfo : {
            title : "Category Details",
            flagKey : "",
            flagValue : "",
            sections: [
              {title : "Category Details", section : "categoryDetails", objectType: "formFieldsEntities", excludes: "category_images "},
            ]
          }

         },

         SubCategory: {

          partialInfoSetup : {
            name : "SubCategory", //Name of the model to pull data from
            queryExtras : [],
            modelTitle: "Sub Category",
            contextRelationShip: "oneToMany", //oneToOne , oneToMany
            status : "formFieldsEntities->categoryDetails->category_status", //if model has no status flags available, leave empty
            fields: [
                {
                  key : "Date Created",
                  value : "created"
                },
                {
                  key : "Name",
                  value : "formFieldsEntities->categoryDetails->name" //example path in the json model to grab the value from. Starts with eith system object or form fields object
                },
                {
                  key : "Description",
                  value : "formFieldsEntities->categoryDetails->description"
                },
                {
                  key : "Date Updated",
                  value : "updated"
                },
                {
                  key : "Status",
                  value : "formFieldsEntities->categoryDetails->category_status"
                },
            ]  
          },

          createModel : true,
          createModelSettings : {
            sections : [ 
              {key : "categoryDetails", label : "Category Details", required : true}            ]
          },

          files: {
            flagKey : "Category Images",
          },

          accodionDisplayInfo : {
            title : "Category Details",
            flagKey : "",
            flagValue : "",
            sections: [
              {title : "Category Details", section : "categoryDetails", objectType: "formFieldsEntities", excludes: "category_images "},
            ]
          }

         },

         SellerProfile: {

          partialInfoSetup : {
            name : "SellerProfile", //Name of the model to pull data from
            queryExtras : [],
            modelTitle: "Seller",
            contextRelationShip: "oneToMany", //oneToOne , oneToMany
            status : "systemManagedEntities->sellerDetails->status", //if model has no status flags available, leave empty
            fields: [
                {
                  key : "Date Created",
                  value : "created"
                },
                {
                  key : "Seller Reference",
                  value : "systemManagedEntities->sellerDetails->shopReference" //example path in the json model to grab the value from. Starts with eith system object or form fields object
                },
                {
                  key : "Phone Number",
                  value : "systemManagedEntities->sellerDetails->sellerContactNumber"
                },
                {
                  key : "Email",
                  value : "systemManagedEntities->sellerDetails->sellerContactEmail"
                },
                {
                  key : "Seller Name",
                  value : "systemManagedEntities->sellerDetails->sellerName"
                },
            ]  
          },

          createModel : false,
          createModelSettings : {
            sections : [ 
              {key : "sellerDetails", label : "Seller Details", required : true}            ]
          },

          // files: {
          //   flagKey : "Product Images",
          // },

          accodionDisplayInfo : {
            title : "Seller Details",
            flagKey : "",
            flagValue : "",
            sections: [
              {title : "Seller Details", section : "sellerDetails", objectType: "systemManagedEntities", excludes: ""},
            ]
          },

          customFunctions: {
            edit: false,
            delete: false
          },

          dynamicFunctions: [
            {
              action: "update-status",
              title: "Update Seller Status",
              options: [
                {label: "Active", val: "Activate"},
                {label: "Banned", val: "Ban User"},
                {label: "Paused", val: "Pause Products"}

              ],
              key : "systemManagedEntities->sellerDetails->status"
            }
          ],

          //only set to true for custom order details view
          viewBackOrderItems : false

         },

         CustomerProfile: {

          partialInfoSetup : {
            name : "CustomerProfile", //Name of the model to pull data from
            queryExtras : [],
            modelTitle: "Customer",
            contextRelationShip: "oneToMany", //oneToOne , oneToMany
            status : "systemManagedEntities->CustomerDetails->status", //if model has no status flags available, leave empty
            fields: [
                {
                  key : "Date Created",
                  value : "created"
                },
                {
                  key : "Customer Reference",
                  value : "systemManagedEntities->CustomerDetails->customerNumber" //example path in the json model to grab the value from. Starts with eith system object or form fields object
                },
                {
                  key : "Customer Name",
                  value : "systemManagedEntities->CustomerDetails->name"
                },
                {
                  key : "Customer Rating",
                  value : "systemManagedEntities->CustomerDetails->rating"
                },
                {
                  key : "Customer Phone",
                  value : "systemManagedEntities->CustomerDetails->phone"
                },
            ]  
          },

          createModel : false,
          createModelSettings : {
            sections : [ 
              {key : "CustomerDetails", label : "Customer Details", required : true}            ]
          },

          // files: {
          //   flagKey : "Product Images",
          // },

          accodionDisplayInfo : {
            title : "Customer Details",
            flagKey : "",
            flagValue : "",
            sections: [
              {title : "Customer Details", section : "CustomerDetails", objectType: "systemManagedEntities", excludes: ""},
            ] 
          }, 
 
          customFunctions: { 
            edit: false, 
            delete: false 
          }, 
 
          dynamicFunctions: [
            {
              action: "update-status",
              title: "Update Customer Status",
              options: [
                {label: "Active", val: "Active"},
                {label: "Banned", val: "Banned"},
                {label: "Paused", val: "Paused"}

              ],
              key : "systemManagedEntities->sellerDetails->status"
            },

            {
              action: "view-context-model",
              title: "View Customer Orders",
              modelName : "BackOrder"
            }
          ],

          //only set to true for custom order details view
          viewBackOrderItems : false

         },

         CourierWaybillQuote: {

          partialInfoSetup : {
            name : "CourierWaybillQuote", //Name of the model to pull data from
            queryExtras : [],
            modelTitle: "Delivery Quote",
            contextRelationShip: "oneToMany", //oneToOne , oneToMany
            status : "systemManagedEntities->parcelDetails->baundaryType", //if model has no status flags available, leave empty
            fields: [
                {
                  key : "Date Created",
                  value : "created"
                },
                {
                  key : "Quote Reference",
                  value : "systemManagedEntities->parcelDetails->quoteReference" //example path in the json model to grab the value from. Starts with eith system object or form fields object
                },
                {
                  key : "Amount",
                  value : "systemManagedEntities->parcelDetails->weightCost"
                },
                {
                  key : "Weight",
                  value : "systemManagedEntities->parcelDetails->weight"
                },
                {
                  key : "Channel",
                  value : "systemManagedEntities->waybillDetails->channel"
                },
            ]  
          },

          createModel : false,
          createModelSettings : {
            sections : [ 
              {key : "orderDetails", label : "Order Details", required : true}            ]
          },

          // files: {
          //   flagKey : "Product Images",
          // },

          accodionDisplayInfo : {
            title : "Order Details",
            flagKey : "",
            flagValue : "",
            sections: [
              {title : "Quote Details", section : "parcelDetails", objectType: "systemManagedEntities", excludes: "seller, items, userId "},
              {title : "Delivery Address", section : "deliveryAddress", objectType: "systemManagedEntities", excludes: ""},
              {title : "Request Details", section : "waybillDetails", objectType: "systemManagedEntities", excludes: ""},
            ]
          },

          customFunctions: {
            edit: false,
            delete: false
          },

          //only set to true for custom order details view
          viewCourierItems : true

         },

         CourierWaybill: {

          partialInfoSetup : {
            name : "CourierWaybill", //Name of the model to pull data from
            queryExtras : [],
            modelTitle: "Delivery Parcel",
            contextRelationShip: "oneToMany", //oneToOne , oneToMany
            status : "systemManagedEntities->waybillDeliveryDetails->status", //if model has no status flags available, leave empty
            fields: [
                {
                  key : "Date Created",
                  value : "created"
                },
                {
                  key : "Quote Reference",
                  value : "systemManagedEntities->parcelDetails->quoteReference" //example path in the json model to grab the value from. Starts with eith system object or form fields object
                },
                {
                  key : "Amount",
                  value : "systemManagedEntities->parcelDetails->weightCost"
                },
                {
                  key : "Weight",
                  value : "systemManagedEntities->parcelDetails->weight"
                },
                {
                  key : "Channel",
                  value : "systemManagedEntities->waybillDetails->channel"
                },
            ]  
          },

          createModel : false,
          createModelSettings : {
            sections : [ 
              {key : "orderDetails", label : "Order Details", required : true}            ]
          },

          // files: {
          //   flagKey : "Product Images",
          // },

          accodionDisplayInfo : {
            title : "Order Details",
            flagKey : "",
            flagValue : "",
            sections: [
              {title : "Parcel Details", section : "parcelDetails", objectType: "systemManagedEntities", excludes: "seller, items, userId "},
              {title : "Delivery Address", section : "deliveryAddress", objectType: "systemManagedEntities", excludes: ""},
              {title : "Request Details", section : "waybillDetails", objectType: "systemManagedEntities", excludes: ""},
              {title : "Delivery Trip Details", section : "waybillDeliveryDetails", objectType: "systemManagedEntities", excludes: ""},
            ]
          },

          customFunctions: {
            edit: false,
            delete: false
          },

          //only set to true for custom order details view
          viewCourierItems : true

         },

         DriverProfile: {

          partialInfoSetup : {
            name : "DriverProfile", //Name of the model to pull data from
            queryExtras : [],
            modelTitle: "Courier/Driver",
            contextRelationShip: "oneToMany", //oneToOne , oneToMany
            status : "systemManagedEntities->driverDetails->onlineStatus", //if model has no status flags available, leave empty
            fields: [
                {
                  key : "Date Created",
                  value : "created"
                },
                {
                  key : "Name",
                  value : "systemManagedEntities->driverDetails->name" //example path in the json model to grab the value from. Starts with eith system object or form fields object
                },
                {
                  key : "Phone Number",
                  value : "systemManagedEntities->driverDetails->phone"
                },
                {
                  key : "Baundary",
                  value : "systemManagedEntities->driverDetails->baundaryName"
                },
                {
                  key : "Status",
                  value : "systemManagedEntities->driverDetails->status"
                },
            ]  
          },

          createModel : false,
          createModelSettings : {
            sections : [ 
              {key : "driverDetails", label : "Driver Details", required : true}            ]
          },

          // files: {
          //   flagKey : "Product Images",
          // },

          accodionDisplayInfo : {
            title : "Courier/Driver Inforation",
            flagKey : "",
            flagValue : "",
            sections: [
              {title : "Driver/Vehicle Details", section : "driverDetails", objectType: "systemManagedEntities", excludes: ""},
            ]
          },

          customFunctions: {
            edit: false,
            delete: false
          },

          dynamicFunctions: [
            {
              action: "update-status",
              title: "Update Driver Status",
              options: [
                {val: "APPROVED", label: "APPROVE"},
                {val: "REJECTED", label: "REJECT"},
                {val: "PAUSED", label: "PAUSE"},
                {val: "FLAGGED", label: "FLAG"}

              ],
              key : "systemManagedEntities->driverDetails->status"
            }
          ],

          //only set to true for custom order details view
          viewBackOrderItems : false

         },

         CourierDeliveryTrip: {

          partialInfoSetup : {
            name : "CourierDeliveryTrip", //Name of the model to pull data from
            queryExtras : [],
            modelTitle: "Delivery Trip",
            contextRelationShip: "oneToMany", //oneToOne , oneToMany
            status : "systemManagedEntities->tripDetails->status", //if model has no status flags available, leave empty
            fields: [
                {
                  key : "Ref Number",
                  value : "systemManagedEntities->tripDetails->referenceNumber"
                },
                {
                  key : "Vehicle RegNumber",
                  value : "systemManagedEntities->tripDetails->vehicleRegNumber" //example path in the json model to grab the value from. Starts with eith system object or form fields object
                },
                {
                  key : "Delivery Fee",
                  value : "systemManagedEntities->tripDetails->quoteAmount"
                },
                {
                  key : "Boundary Type",
                  value : "systemManagedEntities->tripDetails->boundaryType"
                },
                {
                  key : "Status",
                  value : "systemManagedEntities->tripDetails->status"
                },
            ]  
          },

          createModel : false,
          createModelSettings : {
            sections : [ 
              {key : "tripDetails", label : "Trip Details", required : true}            ]
          },

          // files: {
          //   flagKey : "Product Images",
          // },

          accodionDisplayInfo : {
            title : "Trip Details",
            flagKey : "",
            flagValue : "",
            sections: [
              {title : "Delivery Trip Details", section : "tripDetails", objectType: "systemManagedEntities", excludes: ""},
            ]
          },

          customFunctions: {
            edit: false,
            delete: false
          },

          //only set to true for custom order details view
          // viewCourierItems : true

         },

         CashOutTransaction: {

          partialInfoSetup : {
            name : "CashOutTransaction", //Name of the model to pull data from
            queryExtras : [],
            modelTitle: "Cashout",
            contextRelationShip: "oneToMany", //oneToOne , oneToMany
            status : "", //if model has no status flags available, leave empty
            fields: [
                {
                  key : "Request Amount",
                  value : "systemManagedEntities->cashOutDetails->transactionAmount"
                },
                {
                  key : "Deposit Amount",
                  value : "systemManagedEntities->cashOutDetails->amount" //example path in the json model to grab the value from. Starts with eith system object or form fields object
                },
                {
                  key : "Charge Fee",
                  value : "systemManagedEntities->cashOutDetails->transactionCharge"
                },
                {
                  key : "Bank reference",
                  value : "systemManagedEntities->cashOutDetails->bankReference"
                },
                {
                  key : "Transaction reference",
                  value : "systemManagedEntities->cashOutDetails->cashOutreference"
                },
            ]  
          },

          createModel : false,
          createModelSettings : {
            sections : [ 
              {key : "cashOutDetails", label : "Cashout Details", required : true}            
            ]
          },

          // files: {
          //   flagKey : "Product Images",
          // },

          accodionDisplayInfo : {
            title : "Cashout Details",
            flagKey : "",
            flagValue : "",
            sections: [
              {title : "Cashout Details", section : "cashOutDetails", objectType: "systemManagedEntities", excludes: ""},
            ]
          },

          customFunctions: {
            edit: false,
            delete: false
          },

          //only set to true for custom order details view
          // viewCourierItems : true

         },

         BankBulkPaymentsPayoutTransaction: {

          partialInfoSetup : {
            name : "BankBulkPaymentsPayoutTransaction", //Name of the model to pull data from
            queryExtras : [],
            modelTitle: "EFT Pay Slip",
            contextRelationShip: "oneToMany", //oneToOne , oneToMany
            status : "systemManagedEntities->paymentDetails->status", //if model has no status flags available, leave empty
            fields: [
                {
                  key : "Date Created",
                  value : "created"
                },
                {
                  key : "Processed Payouts",
                  value : "systemManagedEntities->paymentDetails->numberOfPayouts" 
                },
                {
                  key : "Total Amount",
                  value : "systemManagedEntities->paymentDetails->debitAmount"
                },
                {
                  key : "Bank Reference",
                  value : "systemManagedEntities->paymentDetails->bankReference"
                },
                {
                  key : "Reference",
                  value : "systemManagedEntities->paymentDetails->paymentReference"
                },
            ]  
          },

          createModel : false,
          createModelSettings : {
            sections : [ 
              {key : "paymentDetails", label : "Payment Details", required : true}            
            ]
          },

          // files: {
          //   flagKey : "Product Images",
          // },

          accodionDisplayInfo : {
            title : "Bulk EFT Payment",
            flagKey : "",
            flagValue : "",
            sections: [
              {title : "Bank Payment Details", section : "paymentDetails", objectType: "systemManagedEntities", excludes: ""},
            ]
          },

          customFunctions: {
            edit: false,
            delete: false
          },

          dynamicFunctions: [
            {
              action: "view-resource-link",
              title: "View Payment Slip PDF",
              button : {
                action : "resorce-link", //model-view or resorce-link
                resorce_link : "/admin-view-bulk-payouts-pdf"
              }
            },

            {
              action: "confirm-payout-payment",
              title: "Confirm Bank Payment"
            }
          ],

          

          //only set to true for custom order details view
          // viewCourierItems : true

         },




         userContextModels : {
          models : [
          ]
        }
      },

      landingPageConfig : {

        actions : [
          {
            name : "Conversations report",
            icon : "my_policy.svg",
            description : "View the daily, weekly, monthly or yearly conversations report. ",
            linkText : "View report",
            linkUrl : "/dashboard/reports/Order",
            modal : true,
            modalName : "email" //email, download, form
          },

          {
            name : "Conversations report",
            icon : "profile.svg",
            description : "View the daily, weekly, monthly or yearly conversations report. ",
            linkText : "View report",
            linkUrl : "/dashboard/reports/user/seller",
            modal : false
          },

          {
            name : "Top selling products",
            icon : "my_policy.svg",
            description : "View the daily, weekly, monthly or yearly conversations report.",
            linkText : "View report",
            linkUrl : "/dashboard/model-details",
            modal : false
          },
        ],

        recentModels : {
          //array of recent items to be displayed on the page, each model should list the keys and value to be displayed
          models: [
            'Invoice'
          ],
        },

        dataTable : {
          models : ["Invoice"]
        },

        components: {
            actions : true,
            recentModels : false,
            modelDataTable: true,
            graphLineBar: false,
            graphChartBar: false         
        }

      },

      analytics : {

        botAnalytics : {
           
        }
      }

    
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
