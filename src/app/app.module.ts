import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FlyoutModule } from 'ngx-flyout';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxEchartsModule } from 'ngx-echarts';
import { AlertModule } from 'ngx-alerts';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

//modules
import { DataTableModule } from './modules/data-table/data-table.module';
import { DynamicFormModule } from './modules/dynamic-form/dynamic-form.module';
import { AnalyticsModule } from './modules/analytics/analytics.module';
import { AppRoutingModule } from './app-routing.module';

//form modukes
//needed modules
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { AuthInterceptor } from './_interceptors/auth.interceptor';
import { AuthGuard } from './_services/auth.guard';
import { MDBBootstrapModule } from 'angular-bootstrap-md';


//services
import { ValidatorService } from './_services/validator.service';
import { AuthService } from './_services/auth.service';
import { CommonDataService } from './_services/common-data.service';

import { AppComponent } from './app.component';
import { MainLayoutComponent } from './view-components/main-layout/main-layout.component';
import { ScreenContentComponent } from './view-components/screen-content/screen-content.component';
import { SideBarMenuComponent } from './view-components/side-bar-menu/side-bar-menu.component';
import { HeaderSectionComponent } from './view-components/header-section/header-section.component';
// import { DashboardScreenComponent } from './system-pages/layouts/dashboard-screen/dashboard-screen.component';

import { SassHelperComponent } from './_providers/sass-helper/sass-helper';
import { ActionsAccordionComponent } from './_partials/system/actions-accordion/actions-accordion.component';
import { ModelsAccordionsComponent } from './_partials/system/models-accordions/models-accordions.component';
import { ContextBreadcrumbsComponent } from './_partials/system/context-breadcrumbs/context-breadcrumbs.component';
import { FilesSectionComponent } from './_partials/system/files-section/files-section.component';
import { ModelDataSummaryViewComponent } from './_partials/system/model-data-summary-view/model-data-summary-view.component';
import { ItemHistorySummaryComponent } from './_partials/system/item-history-summary/item-history-summary.component';
import { ModelDetailsWithHistoryComponent } from './system-pages/application-pages/model-details-with-history/model-details-with-history.component';
import { DashboardLandingPageComponent } from './system-pages/application-pages/dashboard-landing-page/dashboard-landing-page.component';
import { UserContextModelsComponent } from './system-pages/application-pages/user-context-models/user-context-models.component';
import { EmailModelDetailsComponent } from './_partials/system/modals/email-model-details/email-model-details.component';
import { DownloadModelDetailsComponent } from './_partials/system/modals/download-model-details/download-model-details.component';
import { DynamicFormModalComponent } from './_partials/system/modals/dynamic-form-modal/dynamic-form-modal.component';
import { EmailSuccessComponent } from './_partials/system/modals/email-success/email-success.component';
import { BlankLayoutComponent } from './view-components/blank-layout/blank-layout.component';
import { LoginPageComponent } from './system-pages/auth-pages/login-page/login-page.component';
import { ModelListViewComponent } from './system-pages/application-pages/model-list-view/model-list-view.component';
import { ModelListBreadcrumbsComponent } from './_partials/system/model-list-breadcrumbs/model-list-breadcrumbs.component';
import { SpinnerCircleComponent } from './_partials/loaders/spinner-circle/spinner-circle.component';
import { ContextModelItemDetialLinkComponent } from './_partials/system/context-model-item-detial-link/context-model-item-detial-link.component';
import { FormWizardComponent } from './_partials/system/form-wizard/form-wizard.component';
import { AddModelComponent } from './system-pages/application-pages/add-model/add-model.component';
import { AppDragDropDirective } from './_partials/directives/app-drag-drop.directive';
import { InvoiceUpdateComponent } from './invoice-app/invoice-update/invoice-update.component';
import { InvoiceCreteNewComponent } from './invoice-app/invoice-crete-new/invoice-crete-new.component';
import { CreateNewProductComponent } from './apps/e-coomerce-app/create-new-product/create-new-product.component';
import { EcommerceSalesStatsComponent } from './apps/e-coomerce-app/ecommerce-sales-stats/ecommerce-sales-stats.component';
import { CreateNewCategoryComponent } from './apps/e-coomerce-app/create-new-category/create-new-category.component';
import { DashboardOfEnabledmodulesComponent } from './system-pages/application-pages/dashboard-of-enabledmodules/dashboard-of-enabledmodules.component';
import { CardCourseDetailsComponent } from './_partials/system/cards/card-course-details/card-course-details.component';
import { CardAppModuleLaunchComponent } from './_partials/system/cards/card-app-module-launch/card-app-module-launch.component';
import { CardModuleComponentComponent } from './_partials/system/cards/card-module-component/card-module-component.component';
import { ECommerceLandingComponent } from './system-pages/modules-pages/e-commerce/e-commerce-landing/e-commerce-landing.component';
import { CourierLandingPageComponent } from './system-pages/modules-pages/courier/courier-landing-page/courier-landing-page.component';
import { DynamicMenuSliderCardComponent } from './_partials/system/cards/dynamic-menu-slider-card/dynamic-menu-slider-card.component';
import { CatalogueDepartmentsComponent } from './system-pages/modules-pages/e-commerce/catalogue-departments/catalogue-departments.component';
import { CatalogueDashboardComponent } from './system-pages/modules-pages/e-commerce/catalogue-dashboard/catalogue-dashboard.component';
import { UpdateModelStatusComponent } from './_partials/system/dynamic-functions-modals/update-model-status/update-model-status.component';
import { FlagModelWithReasonComponent } from './_partials/system/dynamic-functions-modals/flag-model-with-reason/flag-model-with-reason.component';
import { UnflagModelWithReasonComponent } from './_partials/system/dynamic-functions-modals/unflag-model-with-reason/unflag-model-with-reason.component';
import { DynamicFunctionsDropdownhandlerComponent } from './_partials/system/dynamic-functions-dropdownhandler/dynamic-functions-dropdownhandler.component';
import { ViewOrderItemsComponent } from './_partials/e-commerce/view-order-items/view-order-items.component';
import { ViewSaleItemsComponent } from './_partials/e-commerce/view-sale-items/view-sale-items.component';
import { ReceiveInboundStockComponent } from './system-pages/modules-pages/e-commerce/receive-inbound-stock/receive-inbound-stock.component';
import { WaybillParcelItemsComponent } from './_partials/e-commerce/waybill-parcel-items/waybill-parcel-items.component';
import { DynamicFunctionsFooterbuttonsComponent } from './_partials/system/dynamic-functions-footerbuttons/dynamic-functions-footerbuttons.component';
import { ViewObjectKeyvalueHelperComponent } from './_partials/system/modals/view-object-keyvalue-helper/view-object-keyvalue-helper.component';
import { CourierParcelItemsComponent } from './_partials/courier/courier-parcel-items/courier-parcel-items.component';
import { AccountsDashboardComponent } from './system-pages/modules-pages/accounts/accounts-dashboard/accounts-dashboard.component';
import { WalletCashoutRequestsComponent } from './system-pages/modules-pages/accounts/wallet-cashout-requests/wallet-cashout-requests.component';
import { WalletSettleCashoutsComponent } from './system-pages/modules-pages/accounts/wallet-settle-cashouts/wallet-settle-cashouts.component';
import { AccountsBalanceSummaryComponent } from './system-pages/modules-pages/accounts/accounts-balance-summary/accounts-balance-summary.component';
import { AccountsSummaryViewComponent } from './_partials/e-commerce/accounts-summary-view/accounts-summary-view.component';
import { AdminDriverReviewComponent } from './_partials/courier/admin-driver-review/admin-driver-review.component';
import { CreateBankPayslipModalComponent } from './_partials/e-commerce/modals/create-bank-payslip-modal/create-bank-payslip-modal.component';
import { PayoutProofOfPaymentComponent } from './_partials/e-commerce/modals/payout-proof-of-payment/payout-proof-of-payment.component';
import { ViewSellerDashboardComponent } from './apps/e-commerce-app/view-seller-dashboard/view-seller-dashboard.component';
import { CurrentBusinessAccountBannerComponent } from './_partials/current-business-account-banner/current-business-account-banner.component';

@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
    ScreenContentComponent,
    SideBarMenuComponent,
    HeaderSectionComponent,
    // DashboardScreenComponent,
    SassHelperComponent,
    ActionsAccordionComponent,
    ModelsAccordionsComponent,
    ContextBreadcrumbsComponent,
    FilesSectionComponent,
    ModelDataSummaryViewComponent,
    ItemHistorySummaryComponent,
    ModelDetailsWithHistoryComponent,
    DashboardLandingPageComponent,
    UserContextModelsComponent,
    EmailModelDetailsComponent,
    DownloadModelDetailsComponent,
    DynamicFormModalComponent,
    EmailSuccessComponent,
    BlankLayoutComponent,
    LoginPageComponent,
    ModelListViewComponent,
    ModelListBreadcrumbsComponent,
    SpinnerCircleComponent,
    ContextModelItemDetialLinkComponent,
    FormWizardComponent,
    AddModelComponent,
    AppDragDropDirective,
    InvoiceUpdateComponent,
    InvoiceCreteNewComponent,
    CreateNewProductComponent,
    EcommerceSalesStatsComponent,
    CreateNewCategoryComponent,
    DashboardOfEnabledmodulesComponent,
    CardCourseDetailsComponent,
    CardAppModuleLaunchComponent,
    CardModuleComponentComponent,
    ECommerceLandingComponent,
    CourierLandingPageComponent,
    DynamicMenuSliderCardComponent,
    CatalogueDepartmentsComponent,
    CatalogueDashboardComponent,
    UpdateModelStatusComponent,
    FlagModelWithReasonComponent,
    UnflagModelWithReasonComponent,
    DynamicFunctionsDropdownhandlerComponent,
    ViewOrderItemsComponent,
    ViewSaleItemsComponent,
    ReceiveInboundStockComponent,
    WaybillParcelItemsComponent,
    DynamicFunctionsFooterbuttonsComponent,
    ViewObjectKeyvalueHelperComponent,
    CourierParcelItemsComponent,
    AccountsDashboardComponent,
    WalletCashoutRequestsComponent,
    WalletSettleCashoutsComponent,
    AccountsBalanceSummaryComponent,
    AccountsSummaryViewComponent,
    AdminDriverReviewComponent,
    CreateBankPayslipModalComponent,
    PayoutProofOfPaymentComponent,
    ViewSellerDashboardComponent,
    CurrentBusinessAccountBannerComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    AppRoutingModule,
    FlyoutModule,
    DataTableModule,
    DynamicFormModule,
    AnalyticsModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDropzoneModule,
    NgxEchartsModule,
    TagInputModule, 
    BrowserAnimationsModule,
    HttpClientModule,
    NgxDatatableModule,
    MDBBootstrapModule.forRoot(),
    AlertModule.forRoot({maxMessages: 5, timeout: 5000, position: 'right'})
  ],
  providers: [
    ValidatorService,
    AuthService,
    CommonDataService,
    AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  entryComponents: [
    EmailModelDetailsComponent,
    DownloadModelDetailsComponent,
    DynamicFormModalComponent,
    EmailSuccessComponent,
    UpdateModelStatusComponent,
    FlagModelWithReasonComponent,
    UnflagModelWithReasonComponent,
    CreateBankPayslipModalComponent,
    PayoutProofOfPaymentComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
