import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceCreteNewComponent } from './invoice-crete-new.component';

describe('InvoiceCreteNewComponent', () => {
  let component: InvoiceCreteNewComponent;
  let fixture: ComponentFixture<InvoiceCreteNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceCreteNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceCreteNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
