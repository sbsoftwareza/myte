import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ICustomer, IInvoice, Invoice, Product, IProduct } from '../models';
import { CommonDataService } from '../../_services/common-data.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-invoice-crete-new',
  templateUrl: './invoice-crete-new.component.html',
  styleUrls: ['./invoice-crete-new.component.scss']
})
export class InvoiceCreteNewComponent implements OnInit {

  form: FormGroup;
  title = 'Add Invoice';
  phoneCustomer: "";
  addressCustomer: "";
  showDiv: true;
  customer: true;

  private selectUndefinedOptionValue: any;
  customerList: ICustomer[];
  productList: IProduct[];
  selectedCustomer = null;
  searchedProducts = [];

  constructor(private _fb: FormBuilder, private _data: CommonDataService, private modalService: NgbModal) { }

  ngOnInit() {

    this.customerList = [
      {
        name: "Test",
        lastname: "Nrian",
        phone: 254444454,
        address: "Address: MoneyMarketsPro | Nelson Mandela Square 2nd Floor, West Towers, Maude Street, Sandton" 
      },

      {
        name: "Tessst",
        lastname: "Nrissan",
        phone: 2544444454,
        address: "hshshshs" 
      }
    ]

    this.productList = [
      {
        name: "Shoe",
        location: "ddsdd",
        price: 758,     
        categoryS: "Shoes"
      },
      {
        name: "Beds",
        location: "ddsdd",
        price: 758,     
        categoryS: "Shoes"
      },

      {
        name: "Buckets",
        location: "ddsdd",
        price: 758,     
        categoryS: "Shoes"
      },

      {
        name: "Rolls",
        location: "ddsdd",
        price: 758,     
        categoryS: "Shoes"
      },

      {
        name: "pillows",
        location: "ddsdd",
        price: 758,     
        categoryS: "Shoes"
      },

      {
        name: "Shirts",
        location: "ddsdd",
        price: 758,     
        categoryS: "Shoes"
      },

      {
        name: "Jackets",
        location: "ddsdd",
        price: 758,     
        categoryS: "Shoes"
      },

      {
        name: "Socks",
        location: "ddsdd",
        price: 758,     
        categoryS: "Shoes"
      },

      {
        name: "Shoed",
        location: "ddsdd",
        price: 758,     
        categoryS: "Shoes"
      }
    ];

    this.initForm();
  }

  initForm(): void {
    this.form = this._fb.group({
      customer: [null, Validators.required],
      productselect: [null, Validators.required],
      duedate: [null, Validators.required],
      refNumber: [null, Validators.required],
      totalPrice: 0,
      purchases: this._fb.array([])
    });

    // initialize stream
    const myFormValueChanges$ = this.form.controls['purchases'].valueChanges;
    // subscribe to the stream
    myFormValueChanges$.subscribe(purchases => this.updatePurchasesAmount(purchases));
  }

  updatePurchasesAmount(purchases: any) {
    const control = <FormArray>this.form.controls['purchases'];
    let totalSum = 0;
    for (let i in purchases) {
      const amount = (purchases[i].quantity * purchases[i].product.price);
      control.at(+i).get('amount').setValue(amount, { onlySelf: true, emitEvent: false });
      // update total price
      totalSum += amount;
    }
    this.form.get('totalPrice').setValue(totalSum);
  }

  purchaseForm(product?: Product): FormGroup {
    const numberPatern = '^[0-9.,]+$';
    return this._fb.group({
      product: [product, Validators.required],
      quantity: [1, [Validators.required, Validators.pattern(numberPatern)]],
      amount: [{ value: 0, disabled: true }],
    });
  }

  money(value: number) {
    return value.toLocaleString('en-ZA', { style: 'currency', currency: 'ZAR', minimumFractionDigits: 2 });
  }

  public addPurchase(product: Product): void {
    const control = <FormArray>this.form.controls['purchases'];

    let add = true;
    for (let i in control.controls) {
      if (control.at(+i).get('product').value.name === product.name) {
        // control.controls[i].get('quantity').setValue(control.controls[i].controls.quantity.value + 1);
        control.at(+i).get('quantity').setValue(control.at(+i).get('quantity').value + 1);
        add = false;
      }
    }

    if (add) {
      control.push(this.purchaseForm(product));
      this.showDiv = add;
    }
  }

  private removePurchase(i: number): void {
    const control = <FormArray>this.form.controls['purchases'];
    control.removeAt(i);
  }

  resetPurchase(): void {
    this.form.controls['totalPrice'].setValue(0);
    const control = <FormArray>this.form.controls['purchases'];
    control.controls = [];

  }

  saveProduct() {
    if (this.form.valid && this.form.controls['totalPrice'].value > 0) {

      const result: IInvoice = <IInvoice>this.form.value;
      // Do useful stuff with the gathered data
      console.log(result);
      // this.invoiceService.insertInvoice(result);
      this.phoneCustomer = '';
      this.addressCustomer = '';
      this.showDiv = null;

      // this.tostr.success('Successs', 'Invoice Registered');
      this.initForm();
    } else {
      if (this.form.controls['totalPrice'].value <= 0) {
        // this.tostr.error('Error', 'Invoice not value');
      }
      // else (this.tostr.error('Error', 'Invoice No Registered'));
    }
  }

  customerModal() {
    // this.modal.load({
    //   id: 'my-modal',
    //   component: CustomerComponent
    // });
  }

  productModal() {
    // this.modal.load({
    //   id: 'my-modal',
    //   component: ProductComponent
    // });
  }

  getSelectedOptionText(event) {
    this.resetPurchase();
    this.showDiv = null;
    this.phoneCustomer = event.phone;
    this.addressCustomer = event.address;
    this.selectedCustomer = event;
  }

  onSearch(event) {
    //alert('search')
    console.log(event)
    if (event != null)
      this.searchedProducts = event
  }

}
