import { Component, OnInit, Input } from '@angular/core';
import { EChartOption } from 'echarts';
import { echartStyles } from '../../echart-styles';
import { ChartsModule } from 'angular-bootstrap-md';

import { CommonDataService } from '../../../../_services/common-data.service';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.scss']
})
export class BasicComponent implements OnInit {

  @Input() chartData;
  @Input() chartName;

  statsData: any;
  charts: any[] = [];

	constructor(private _data: CommonDataService) {

        // Query stats data
        let queryData = new FormData();
        queryData.append('modelName', 'botUser');
        queryData.append('entities', 'Password, Username');
        queryData.append('intents', 'Password, Username, Support');

        _data.dynamicFormPost(queryData, 'bot-analytics').subscribe (
        data => {
            this.statsData = data['analytics'];
            //console.log(this.statsData);

            Object.keys(this.statsData).forEach(key => {
                let stat = {};
                if (typeof this.statsData[key] === 'object') {
                    // 1. Collect series data

                    let innerObject = this.statsData[key];
                    let dataHolder = [];

                    Object.keys(innerObject).forEach(key => {
                        let innerValue = innerObject[key];
                        var holder = {};
                        holder['value'] = innerValue;
                        holder['name'] = key;
                        dataHolder.push(holder)
                    });

                    //stat['name'] = key;
                    if (key === 'entities') { stat['name'] = 'Common User Issues'; }
                    else if (key === 'intents') { stat['name'] = 'User Objectives';}
                    else if (key === 'status') { stat['name'] = 'Chat Statuses'; }
                    else { stat['name'] = key; }

                    stat['type'] = 'pie';
                    stat['radius'] = '75%';
                    stat['center'] = ['50%', '50%'];
                    stat['data'] = dataHolder;
                    stat['itemStyle'] = {
                            emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    };

                    // 2. Create series data

                    let seriesHolder = [];
                    seriesHolder.push(stat);

                    // 3. Create chart object
                    let chart = {};
                    chart['color'] = ['rgb(245,160,0)', 'rgb(235,101,36)', 'rgb(131,187,38)', 'rgb(45,146,54)', 'rgb(66,169,224)', 'rgb(135,80,154)', 'rgb(51,70,151)', 'rgb(231,50,138)', 'rgb(227,18,20)'];
                    chart['tooltip'] = {
                        show: true,
                        backgroundColor: 'rgba(0, 0, 0, .8)'
                    };
                    chart['xAxis'] = [{
                        axisLine: {
                            show: false
                        },
                        splitLine: {
                            show: false
                        }
                    }];
                    chart['yAxis'] = [{
                        axisLine: {
                            show: false
                        },
                        splitLine: {
                            show: false
                        }
                    }];
                    chart['series'] = seriesHolder;

                    this.charts.push(chart)
                }
            });
        },
        err => {},
        () => {},
        )
    }

	ngOnInit() {
	}

}
