import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxEchartsModule } from 'ngx-echarts';
import { BasicComponent } from './pie-charts/basic/basic.component';

@NgModule({
  declarations: [BasicComponent],
  imports: [
    CommonModule,
    NgxEchartsModule
  ],
  exports:      [ BasicComponent ]
})
export class AnalyticsModule { }
