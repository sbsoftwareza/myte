import { TestBed } from '@angular/core/testing';

import { DynamicFormDataHandlerService } from './dynamic-form-data-handler.service';

describe('DynamicFormDataHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DynamicFormDataHandlerService = TestBed.get(DynamicFormDataHandlerService);
    expect(service).toBeTruthy();
  });
});
