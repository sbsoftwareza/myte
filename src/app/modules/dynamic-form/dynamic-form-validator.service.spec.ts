import { TestBed } from '@angular/core/testing';

import { DynamicFormValidatorService } from './dynamic-form-validator.service';

describe('DynamicFormValidatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DynamicFormValidatorService = TestBed.get(DynamicFormValidatorService);
    expect(service).toBeTruthy();
  });
});
