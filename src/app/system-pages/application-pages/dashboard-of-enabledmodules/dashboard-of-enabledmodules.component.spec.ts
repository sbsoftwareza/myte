import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardOfEnabledmodulesComponent } from './dashboard-of-enabledmodules.component';

describe('DashboardOfEnabledmodulesComponent', () => {
  let component: DashboardOfEnabledmodulesComponent;
  let fixture: ComponentFixture<DashboardOfEnabledmodulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardOfEnabledmodulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardOfEnabledmodulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
