import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { environment } from '../../../../environments/environment';

import { CommonDataService } from '../../../_services/common-data.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard-of-enabledmodules',
  templateUrl: './dashboard-of-enabledmodules.component.html',
  styleUrls: ['./dashboard-of-enabledmodules.component.scss']
})
export class DashboardOfEnabledmodulesComponent implements OnInit, AfterViewInit, OnDestroy {

  recentModelsConfig = environment.dashboardConfig.landingPageConfig.recentModels.models;

  dynamicTables = environment.dashboardConfig.landingPageConfig.dataTable.models;

  pageComponents = environment.dashboardConfig.landingPageConfig.components;

  pageReady = false;
  
  // public isCollapsed = false;
  public isCollapsed = true;
  public isCollapsedMessages = true;
  public isCollapsedProfile = true;

  newOrders = 0
  newDeliveries = 0
  newCashouts = 0

  notifications = []
  notificationsCount = 0

  emails = []
  emailsCount = 0

  constructor(private _data: CommonDataService) { }

  ngOnInit() {
    this.getNewOrders()
    this.getNewCourierTrips()
    this.getNewCashouts()
    this.getNewAdminNotifications()
    this.getNewAdminEmails()

  }

  ngAfterViewInit() {
    let test =  this.delay(1200);
    this.pageReady = true;
  }

  ngOnDestroy() {
    this.pageReady = false;
  }

  async delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  getNewOrders() {
    let queryData = new FormData();
    queryData.append('modelName', 'BackOrder');
    queryData.append('orderDetails->status->systemManagedEntities', 'PROCESSING');

    this._data.getModelData(queryData) 
    .pipe(first())
    .subscribe(
  
      data => {
      
        this.newOrders = Object.keys(data['covers']).length;

      },
      error => {},
      () => {}
    );
  }

  getNewCourierTrips() {
    let queryData = new FormData();
    queryData.append('modelName', 'CourierDeliveryTrip');
    queryData.append('tripDetails->status->systemManagedEntities', 'PENDING');

    this._data.getModelData(queryData) 
    .pipe(first())
    .subscribe(
  
      data => {
      
        this.newDeliveries = Object.keys(data['covers']).length;

      },
      error => {},
      () => {}
    );
  }

  getNewCashouts() {
    let queryData = new FormData();
    queryData.append('modelName', 'CashOutTransaction');
    queryData.append('ledger->transactionRef->systemManagedEntities', '');

    this._data.getModelData(queryData) 
    .pipe(first())
    .subscribe(
  
      data => {
      
        this.newCashouts = Object.keys(data['covers']).length;

      },
      error => {},
      () => {}
    );
  }

  getNewAdminNotifications() {
    let queryData = new FormData();
    queryData.append('modelName', 'AdminNotification');

    this._data.getModelData(queryData) 
    .pipe(first())
    .subscribe(
  
      data => {
        
        this.notificationsCount = Object.keys(data['covers']).length;
        this.notifications = data['covers']

      },
      error => {},
      () => {}
    );
  }

  getNewAdminEmails() {
    let queryData = new FormData();
    queryData.append('modelName', 'AdminEmail');

    this._data.getModelData(queryData) 
    .pipe(first())
    .subscribe(
  
      data => {
        
        this.emailsCount = Object.keys(data['covers']).length;
        this.emails = data['covers']

      },
      error => {},
      () => {}
    );
  }

}
