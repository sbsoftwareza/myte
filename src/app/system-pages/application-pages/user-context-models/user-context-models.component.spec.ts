import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserContextModelsComponent } from './user-context-models.component';

describe('UserContextModelsComponent', () => {
  let component: UserContextModelsComponent;
  let fixture: ComponentFixture<UserContextModelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserContextModelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserContextModelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
