import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { environment } from '../../../../environments/environment';

import { CommonDataService } from '../../../_services/common-data.service';

@Component({
  selector: 'app-dashboard-landing-page',
  templateUrl: './dashboard-landing-page.component.html',
  styleUrls: ['./dashboard-landing-page.component.scss']
})

export class DashboardLandingPageComponent implements OnInit, AfterViewInit, OnDestroy {

  recentModelsConfig = environment.dashboardConfig.landingPageConfig.recentModels.models;

  dynamicTables = environment.dashboardConfig.landingPageConfig.dataTable.models;

  pageComponents = environment.dashboardConfig.landingPageConfig.components;

  pageReady = false;
  
  public isCollapsed = false;

  constructor() { }

  ngOnInit() {
    
  }

  ngAfterViewInit() {

    this.pageReady = true;
  }

  ngOnDestroy() {
    this.pageReady = false;
  }

}
