import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelDetailsWithHistoryComponent } from './model-details-with-history.component';

describe('ModelDetailsWithHistoryComponent', () => {
  let component: ModelDetailsWithHistoryComponent;
  let fixture: ComponentFixture<ModelDetailsWithHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelDetailsWithHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelDetailsWithHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
