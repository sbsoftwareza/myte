import { Component, OnInit, Output, AfterViewInit  } from '@angular/core';
import { Router  , ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-add-model',
  templateUrl: './add-model.component.html',
  styleUrls: ['./add-model.component.scss']
})
export class AddModelComponent implements OnInit, AfterViewInit {

  pageReady = false;
  modelName;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe((params) => {

      if(params['modelName']) {
        // this.modelName = params['modelName'];
        if(this.modelName != params['modelName']) {
          this.modelName = params['modelName'];
        }
      } 
    });
  }

  ngAfterViewInit() {
    this.pageReady = true;
  }

}
