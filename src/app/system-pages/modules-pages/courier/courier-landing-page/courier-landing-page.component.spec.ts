import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourierLandingPageComponent } from './courier-landing-page.component';

describe('CourierLandingPageComponent', () => {
  let component: CourierLandingPageComponent;
  let fixture: ComponentFixture<CourierLandingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourierLandingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourierLandingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
