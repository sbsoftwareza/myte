import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-courier-landing-page',
  templateUrl: './courier-landing-page.component.html',
  styleUrls: ['./courier-landing-page.component.scss']
})
export class CourierLandingPageComponent implements OnInit, AfterViewInit, OnDestroy {

  pageReady = false;
  useFlyingCards = false;

  cards = [
    {
      title : "QUOTES",
      description: "View Delivery Quotes",
      routerLink: "dashboard/model-list/CourierWaybillQuote",
      icon: "fa fa-shopping-basket"
    },

    {
      title : "DELIVERY REQUESTS",
      description: "View All Delivery Requests",
      // routerLink: "dashboard/model-list/Product",
      routerLink: "dashboard/model-list/CourierWaybill",
      icon: "fa fa-tags"
    },

    {
      title : "WAYBILL TRIPS",
      description: "View All Deliveries",
      routerLink: "dashboard/model-list/CourierDeliveryTrip",
      icon: "fa fa-truck"
    },

    {
      title : "DRIVERS",
      description: "View Drivers",
      routerLink: "dashboard/model-list/DriverProfile",
      icon: "fa fa-users"
    }
  ]

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    let test =  this.delay(1200);
    this.pageReady = true;
  }

  ngOnDestroy() {
    this.pageReady = false;
  }

  async delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }


}
