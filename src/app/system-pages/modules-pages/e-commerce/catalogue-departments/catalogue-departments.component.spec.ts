import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogueDepartmentsComponent } from './catalogue-departments.component';

describe('CatalogueDepartmentsComponent', () => {
  let component: CatalogueDepartmentsComponent;
  let fixture: ComponentFixture<CatalogueDepartmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogueDepartmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogueDepartmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
