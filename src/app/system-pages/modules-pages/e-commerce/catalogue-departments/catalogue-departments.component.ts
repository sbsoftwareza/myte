import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { Router  , ActivatedRoute } from '@angular/router';
import { CommonDataService } from '../../../../_services/common-data.service';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-catalogue-departments',
  templateUrl: './catalogue-departments.component.html',
  styleUrls: ['./catalogue-departments.component.scss']
})
export class CatalogueDepartmentsComponent implements OnInit, AfterViewInit, OnDestroy  {

  dataReady;
  pageReady = false;
  pageReload = false;
  models = [];
  modelName = "Departments"
  modelData
  

  constructor(private route: ActivatedRoute,private router: Router,private _data: CommonDataService) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    let test =  this.delay(1200);
    this.pageReady = true;
  }

  ngOnDestroy() {
    this.pageReady = false;
  }

  async delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }


  queryModelFromApi() {

    let queryData = new FormData();
    queryData.append('modelName', this.modelName);
  
  
    this._data.getModelData(queryData) 
    .pipe(first())
    .subscribe(
  
      data => {
        
        this.modelData = data['covers'];

        for (let index = 0; index < this.modelData.length; index++) {
          const element = this.modelData[index];

          let cardData = {

            title : element['cover']['model']['modelData']['formFieldsEntities']['departmentDetails']['name'],
            description: "",
            routerLink: "dashboard/e-commerce/categories/"+element['id'],
            icon: "fa fa-shopping-basket"
          }
          
        }

        this.dataReady = true;

  
      },
  
      error => {

      },
  
      () => {
      }
    );
  
  }

}
