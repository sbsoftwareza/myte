import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ECommerceLandingComponent } from './e-commerce-landing.component';

describe('ECommerceLandingComponent', () => {
  let component: ECommerceLandingComponent;
  let fixture: ComponentFixture<ECommerceLandingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ECommerceLandingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ECommerceLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
