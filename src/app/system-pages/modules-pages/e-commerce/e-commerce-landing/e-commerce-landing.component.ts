import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-e-commerce-landing',
  templateUrl: './e-commerce-landing.component.html',
  styleUrls: ['./e-commerce-landing.component.scss']
})
export class ECommerceLandingComponent implements OnInit, AfterViewInit, OnDestroy {

  pageReady = false;
  useFlyingCards = false;

  cards = [
    {
      title : "BACKORDERS",
      description: "",
      routerLink: "dashboard/model-list/BackOrder",
      icon: "fa fa-shopping-basket"
    },

    {
      title : "CATALOGUE",
      description: "",
      // routerLink: "dashboard/model-list/Product",
      routerLink: "dashboard/ecommerce-modules/catalogue-dashboard",
      icon: "fa fa-tags"
    },

    // {
    //   title : "COLLECTIONS",
    //   description: "",
    //   routerLink: "dashboard/model-list/Product",
    //   icon: "fa fa-shopping-bag"
    // },

    {
      title : "INBOUND STOCK",
      description: "",
      routerLink: "dashboard/model-list/INBOUNDWAYBILL",
      icon: "fa fa-truck"
    },

    {
      title : "OUTBOUND STOCK",
      description: "",
      routerLink: "dashboard/model-list/OUTBOUNDWAYBILL",
      icon: "fa fa-shopping-bag"
    },

    // {
    //   title : "READY PARCELS",
    //   description: "",
    //   routerLink: "dashboard/model-list/INBOUNDWAYBILL",
    //   icon: "fa fa-thumbs-up"
    // },

    {
      title : "SELLERS",
      description: "",
      routerLink: "dashboard/model-list/SellerProfile",
      icon: "fa fa-user-secret"
    },

    {
      title : "CUSTOMERS",
      description: "",
      routerLink: "dashboard/model-list/CustomerProfile",
      icon: "fa fa-users"
    }
  ]

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    let test =  this.delay(1200);
    this.pageReady = true;
  }

  ngOnDestroy() {
    this.pageReady = false;
  }

  async delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }


}
