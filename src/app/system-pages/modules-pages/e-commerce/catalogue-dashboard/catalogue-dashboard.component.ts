import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-catalogue-dashboard',
  templateUrl: './catalogue-dashboard.component.html',
  styleUrls: ['./catalogue-dashboard.component.scss']
})
export class CatalogueDashboardComponent implements OnInit, AfterViewInit, OnDestroy {

  pageReady = false;
  useFlyingCards = false

  cards = [
    {
      title : "Departments",
      description: "",
      routerLink: "dashboard/model-list/Departments",
      icon: "fa fa-clipboard"
    },

    {
      title : "Categories",
      description: "",
      routerLink: "dashboard/model-list/ShopCategory",
      icon: "fa fa-th-large"
    },

    {
      title : "Sub-Categories",
      description: "",
      routerLink: "dashboard/model-list/SubCategory",
      icon: "fa fa-align-center"
    },

    {
      title : "All Products",
      description: "",
      routerLink: "dashboard/model-list/Product",
      icon: "fa fa-tags"
    }
  ]

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    let test =  this.delay(1200);
    this.pageReady = true;
  }

  ngOnDestroy() {
    this.pageReady = false;
  }

  async delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

}
