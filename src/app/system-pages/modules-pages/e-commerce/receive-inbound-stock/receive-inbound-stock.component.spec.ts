import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiveInboundStockComponent } from './receive-inbound-stock.component';

describe('ReceiveInboundStockComponent', () => {
  let component: ReceiveInboundStockComponent;
  let fixture: ComponentFixture<ReceiveInboundStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiveInboundStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiveInboundStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
