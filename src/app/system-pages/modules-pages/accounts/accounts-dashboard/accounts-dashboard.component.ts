import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateBankPayslipModalComponent } from 'src/app/_partials/e-commerce/modals/create-bank-payslip-modal/create-bank-payslip-modal.component';

@Component({
  selector: 'app-accounts-dashboard',
  templateUrl: './accounts-dashboard.component.html',
  styleUrls: ['./accounts-dashboard.component.scss']
})
export class AccountsDashboardComponent implements OnInit {

  pageReady = false;
  useFlyingCards = false;

  cards = [
    {
      title : "Account Balances",
      description: "View Delivery Quotes",
      routerLink: "dashboard/accounts/balances",
      icon: "fa fa-balance-scale"
    },

    {
      title : "Payout requests",
      description: "View All Delivery Requests",
      // routerLink: "dashboard/model-list/Product",
      routerLink: "dashboard/model-list/CashOutTransaction",
      icon: "fa fa-credit-card"
    },

    {
      title : "Bank Payslips",
      description: "View All Deliveries",
      routerLink: "dashboard/model-list/BankBulkPaymentsPayoutTransaction",
      icon: "fa fa-briefcase"
    },

    // {
    //   title : "Create Bank Payslip",
    //   description: "View Drivers",
    //   routerLink: "dashboard/model-list/DriverProfile",
    //   icon: "fa fa-check-circle"
    // }
  ]

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    let test =  this.delay(1200);
    this.pageReady = true;
  }

  ngOnDestroy() {
    this.pageReady = false;
  }

  async delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  createBankPayslips() {
    const modalRef = this.modalService.open(CreateBankPayslipModalComponent);
  }


}

