import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountsBalanceSummaryComponent } from './accounts-balance-summary.component';

describe('AccountsBalanceSummaryComponent', () => {
  let component: AccountsBalanceSummaryComponent;
  let fixture: ComponentFixture<AccountsBalanceSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountsBalanceSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountsBalanceSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
