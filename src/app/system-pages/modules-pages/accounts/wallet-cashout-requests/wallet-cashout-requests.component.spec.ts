import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WalletCashoutRequestsComponent } from './wallet-cashout-requests.component';

describe('WalletCashoutRequestsComponent', () => {
  let component: WalletCashoutRequestsComponent;
  let fixture: ComponentFixture<WalletCashoutRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WalletCashoutRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WalletCashoutRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
