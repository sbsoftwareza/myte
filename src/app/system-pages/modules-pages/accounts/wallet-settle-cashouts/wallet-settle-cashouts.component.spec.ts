import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WalletSettleCashoutsComponent } from './wallet-settle-cashouts.component';

describe('WalletSettleCashoutsComponent', () => {
  let component: WalletSettleCashoutsComponent;
  let fixture: ComponentFixture<WalletSettleCashoutsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WalletSettleCashoutsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WalletSettleCashoutsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
