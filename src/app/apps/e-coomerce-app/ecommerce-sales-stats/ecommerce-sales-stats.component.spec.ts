import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EcommerceSalesStatsComponent } from './ecommerce-sales-stats.component';

describe('EcommerceSalesStatsComponent', () => {
  let component: EcommerceSalesStatsComponent;
  let fixture: ComponentFixture<EcommerceSalesStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcommerceSalesStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcommerceSalesStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
