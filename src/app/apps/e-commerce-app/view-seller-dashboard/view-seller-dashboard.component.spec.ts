import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSellerDashboardComponent } from './view-seller-dashboard.component';

describe('ViewSellerDashboardComponent', () => {
  let component: ViewSellerDashboardComponent;
  let fixture: ComponentFixture<ViewSellerDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSellerDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSellerDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
