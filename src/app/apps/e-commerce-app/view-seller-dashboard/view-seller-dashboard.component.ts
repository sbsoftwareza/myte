import { Component, OnInit } from '@angular/core';


import { first } from 'rxjs/operators';
import { Router, ActivatedRoute , ParamMap} from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms';
//import { UserService } from '../../services/user.service';
import { CommonDataService } from '../../../_services/common-data.service';
import { CommerceDataService } from '../../../_services/commerce-data.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-view-seller-dashboard',
  templateUrl: './view-seller-dashboard.component.html',
  styleUrls: ['./view-seller-dashboard.component.scss']
})
export class ViewSellerDashboardComponent implements OnInit {

  pageName = "My Business Profile";

  saleThisMonth = 0;
  salesTotal = 0;
  totalSales = 0;
  newSales = 0;
  completedSales = 0;
  soldProducts = 0;
  deliveries = 0
  sellerProducts = 0
  readyToSendToWarehouse = 0
  profile 
  
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private _data: CommonDataService,
    private route: ActivatedRoute,
    private _ecomm: CommerceDataService
  ) { }

  ngOnInit() {
    let context_id = sessionStorage.getItem('context')

    if(context_id != undefined) {
      // this.getOrders(context_id)
      // this.getDeliveries(context_id)
      // this.getSellerProducts(context_id)
      this.getSellerProfile(context_id)
    }
  }

  createProduct(id)
  {
    this.queryParamsSet({mid: id});
  	this.router.navigate(['/sellers/account/create-product']);
  }

  queryParamsSet(dataParams: any) {
    this._data.queryParamsChanged.next(dataParams);
  }

  getSellerProfile(context_id) {
    let queryData = new FormData();
    queryData.append('modelName', "SellerProfile");
    queryData.append('context_id', context_id);

    //lets get the model for this page
    this._data.getModelData(queryData) 
    .pipe(first())
    .subscribe(
      data => {
        
        if(data['covers'].length > 0) {
          //count
          this.profile = data['covers'][0]

          this.getOrders(context_id)
          // this.getDeliveries(context_id)
          this.getSellerProducts(context_id)
          
        }

        else {
          this.router.navigate(['/sellers/register/user-details']);
        }

      },
      error => {
        this.router.navigate(['/sellers/register/user-details']);
      },
      () => {
      }
    );
  }

  getOrders(context) {
    let queryData = new FormData();
    queryData.append('modelName', "Sale");
    queryData.append('context_id', context);

    //lets get the model for this page
    this._data.getModelData(queryData) 
    .pipe(first())
    .subscribe(
      data => {
        
        if(data['covers'].length > 0) {
          //count
          this.totalSales = data['covers'].length

          data['covers'].forEach(element => {
            this.salesTotal += element['cover']['model']['modelData']['systemManagedEntities']['saleDetails']['totalAmount']
          });

          let allCompletedOrders = this.filterCompletedOrders(data['covers'])
          this.completedSales = allCompletedOrders.length

          let newOrders = this.filterNewSales(data['covers'])
          this.newSales = newOrders.length

          let toCollect = this.filterSalesReadyToCollect(data['covers'])
          this.deliveries = toCollect.length

          let toDeliver = this.filterSalesReadyToSend(data['covers'])
          this.readyToSendToWarehouse = toDeliver.length
          
        }

      },
      error => {
      },
      () => {
      }
    );
  }

  filterCompletedOrders(orders) {
    return orders.filter(order => order['cover']['model']['modelData']['systemManagedEntities']['saleDetails']['collectionFromSellerStatus'] == 'DELIVERED');
  }

  filterNewSales(orders) {
    // return this.modelData
    return orders.filter(order => order['cover']['model']['modelData']['systemManagedEntities']['saleDetails']['collectionFromSellerStatus'] === "PENDING"); 
  }

  filterSalesReadyToCollect(orders) {
    // return this.modelData
    return orders.filter(order => order['cover']['model']['modelData']['systemManagedEntities']['saleDetails']['collectionFromSellerStatus'] === "READYFORCOLLECTION"); 
  }
  
  filterSalesReadyToSend(orders) {
    // return this.modelData
    return orders.filter(order => order['cover']['model']['modelData']['systemManagedEntities']['saleDetails']['collectionFromSellerStatus'] === "READYFORDELIVERY"); 
  }

  getDeliveries(context) {
    let queryData = new FormData();
    queryData.append('modelName', "INBOUNDWAYBILL");
    queryData.append('context_id', context);

    //lets get the model for this page
    this._data.getModelData(queryData) 
    .pipe(first())
    .subscribe(
      data => {
        
        if(data['covers'].length > 0) {
            //count
            this.deliveries = data['covers'].length
          }

      },
      error => {
      },
      () => {
      }
    );
  }

  getSellerProducts(context) {
    let queryData = new FormData();
    queryData.append('modelName', "Product");
    queryData.append('context_id', context);

    //lets get the model for this page
    this._data.getModelData(queryData) 
    .pipe(first())
    .subscribe(
      data => {
        
        if(data['covers'].length > 0) {
            //count
            this.sellerProducts = data['covers'].length
          }

      },
      error => {
      },
      () => {
      }
    );
  }

  getSellerCollections(context) {
    let queryData = new FormData();
    queryData.append('modelName', "Sale");
    queryData.append('context_id', context);

    //lets get the model for this page
    this._data.getModelData(queryData) 
    .pipe(first())
    .subscribe(
      data => {
        
        if(data['covers'].length > 0) {
            //count
          }

      },
      error => {
      },
      () => {
      }
    );
  }

  getWalletBalance() {

  }

}
