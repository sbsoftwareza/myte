import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms';
import { first } from 'rxjs/operators';

//import { UserService } from '../../services/user.service';
import { CommonDataService } from '../../_services/common-data.service';

declare var $:any;
declare var iziToast:any;

@Component({
  selector: 'app-current-business-account-banner',
  templateUrl: './current-business-account-banner.component.html',
  styleUrls: ['./current-business-account-banner.component.css']
})
export class CurrentBusinessAccountBannerComponent implements OnInit {

  firstname:any;
  lastname:any;
  date_joined:any;

  localModelDataStorage = false;
  modelName = 'User';
  modelData: any;
  avatar:any;
  points = 0;


  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private _data: CommonDataService
  ) { 
    //lets get the model for this page
        this._data.getProfile() 
        .pipe(first())
        .subscribe(
          data => {

            console.log(data)
            this.modelData = data['cover']['model']['modelData']['formFieldsEntities'];

            this.firstname = data['cover']['model']['modelData']['formFieldsEntities']['first_name'];
            this.lastname = data['cover']['model']['modelData']['formFieldsEntities']['last_name'];
            this.date_joined = data['cover']['created'];
            this.avatar = data['cover']['model']['modelData']['formFieldsEntities']['avatar'][0]['tmp_name'];

            if(data['cover']['model']['modelData']['formFieldsEntities']['points'])
              this.points = data['cover']['model']['modelData']['formFieldsEntities']['points'];
          },
          error => {},
          () => {}
        );
  }

  ngOnInit() {
  }

}
