import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentBusinessAccountBannerComponent } from './current-business-account-banner.component';

describe('CurrentBusinessAccountBannerComponent', () => {
  let component: CurrentBusinessAccountBannerComponent;
  let fixture: ComponentFixture<CurrentBusinessAccountBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentBusinessAccountBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentBusinessAccountBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
