import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSaleItemsComponent } from './view-sale-items.component';

describe('ViewSaleItemsComponent', () => {
  let component: ViewSaleItemsComponent;
  let fixture: ComponentFixture<ViewSaleItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSaleItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSaleItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
