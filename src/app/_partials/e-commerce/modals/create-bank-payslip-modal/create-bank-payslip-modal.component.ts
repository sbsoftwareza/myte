import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { CommonDataService } from '../../../../_services/common-data.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-create-bank-payslip-modal',
  templateUrl: './create-bank-payslip-modal.component.html',
  styleUrls: ['./create-bank-payslip-modal.component.scss']
})
export class CreateBankPayslipModalComponent implements OnInit {

  modelForm: FormGroup;

  submitButtonText = "Confirm Pin & Submit"

  debitAmount = 0
  amountRequested = 0
  transactionCharges = 0
  numberOfPayouts = 0

  pageReady = false
  formLabel = "To complete this request you need to provide your admin password"
  completed= false

  constructor(public activeModal: NgbActiveModal, private formBuilder: FormBuilder, private _data: CommonDataService) { }

  ngOnInit() {

    this.getPayoutSummaries()

    this.modelForm = this.formBuilder.group({

      pin: ['', Validators.required], //Product name

    });
  }

  getPayoutSummaries() {

    this._data.dynamicFormPost({}, 'admin-bulk-payouts-summary') 
      .pipe(first())
      .subscribe(

        data => {
          //console.log(data);
          //console.log(this.modelData)
          if(data['paymentDetails'] != null) {

            this.debitAmount = data['paymentDetails']['debitAmount']
            this.amountRequested = data['paymentDetails']['amountRequested']
            this.transactionCharges = data['paymentDetails']['transactionCharges']
            this.numberOfPayouts = data['paymentDetails']['numberOfPayouts']

            
          }

          this.pageReady = true
        }, 

        error => {
          this.pageReady = true

        },

        () => {

        }
      );
  }

  confirmBankPayout() {

    this.submitButtonText = "Please wait..."

    if (this.modelForm.invalid) {

      alert('Please enter a valid pin!')

      //show errors
      this.submitButtonText = "Confirm Pin & Submit"

      return;
    }

    let pin = this.modelForm.controls['pin'].value;

    this._data.dynamicFormPost({"pin": pin}, 'admin-bulk-payouts-create') 
      .pipe(first())
      .subscribe(

        data => {
          //console.log(data);
          //console.log(this.modelData)
          if(data['paymentDetails'] != null) {

            this.debitAmount = data['paymentDetails']['debitAmount']
            this.amountRequested = data['paymentDetails']['amountRequested']
            this.transactionCharges = data['paymentDetails']['transactionCharges']
            this.numberOfPayouts = data['paymentDetails']['numberOfPayouts']

            alert("Bank payment slip has been submitted successfully with Ref: "+data['paymentDetails']['paymentReference'])

            this.formLabel = "Bank payment slip has been submitted successfully with Ref: "+data['paymentDetails']['paymentReference']
            this.completed = true

            // this.activeModal.close('Close click')

            
          }

          this.pageReady = true
        }, 

        error => {
          this.pageReady = true

        },

        () => {

        }
      );

  }

}
