import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBankPayslipModalComponent } from './create-bank-payslip-modal.component';

describe('CreateBankPayslipModalComponent', () => {
  let component: CreateBankPayslipModalComponent;
  let fixture: ComponentFixture<CreateBankPayslipModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBankPayslipModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBankPayslipModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
