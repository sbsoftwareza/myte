import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { CommonDataService } from '../../../../_services/common-data.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-payout-proof-of-payment',
  templateUrl: './payout-proof-of-payment.component.html',
  styleUrls: ['./payout-proof-of-payment.component.scss']
})
export class PayoutProofOfPaymentComponent implements OnInit {

  @Input() data;
  modelForm: FormGroup;

  submitButtonText = "Confirm & Submit"

  constructor(public activeModal: NgbActiveModal, private formBuilder: FormBuilder, private _data: CommonDataService) { }

  ngOnInit() {

    this.modelForm = this.formBuilder.group({

      bankRef: ['', Validators.required], //Product name

    });
  }

  confirmBankReference() {

    this.submitButtonText= 'Please Wait...';

    if (this.modelForm.invalid) {
      
      this.submitButtonText= 'Confirm & Submit';

      return;
    }

    let modelObject1 = new FormData();
    let endpoint = "admin-payout-paymnet-proof"

    modelObject1.append('modelName', this.data.modelName);
    modelObject1.append('id', this.data.modelData['id']);
    modelObject1.append('bankReference', this.modelForm.controls['bankRef'].value);
    // modelObject1.append('systemManagedEntities-cashOutDetails-bankReference', this.modelForm.controls['bankRef'].value);
    this._data.dynamicFormPost(modelObject1, endpoint)
    .pipe(first())
    .subscribe(
  
      data => {
        this.submitButtonText= 'Confirm & Submit';
        alert('Successfully Updated!')
        this.activeModal.close('Close click')
  
      },
  
      error => {
        alert('Error Updating please try again!')
        this.submitButtonText= 'Confirm & Submit';
      },
  
      () => {
      }
    );
  }

}
