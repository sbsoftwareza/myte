import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayoutProofOfPaymentComponent } from './payout-proof-of-payment.component';

describe('PayoutProofOfPaymentComponent', () => {
  let component: PayoutProofOfPaymentComponent;
  let fixture: ComponentFixture<PayoutProofOfPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayoutProofOfPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayoutProofOfPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
