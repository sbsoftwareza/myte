import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountsSummaryViewComponent } from './accounts-summary-view.component';

describe('AccountsSummaryViewComponent', () => {
  let component: AccountsSummaryViewComponent;
  let fixture: ComponentFixture<AccountsSummaryViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountsSummaryViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountsSummaryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
