import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { CommonDataService } from '../../../_services/common-data.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-accounts-summary-view',
  templateUrl: './accounts-summary-view.component.html',
  styleUrls: ['./accounts-summary-view.component.scss']
})
export class AccountsSummaryViewComponent implements OnInit {

  chartReady = false;

  constructor(private _data: CommonDataService, private router: Router) { }

  public chartType: string = 'line';

  public chartDatasets: Array<any> = [
  
  ];

  // { data: [65, 59, 80, 81, 56, 55, 40], label: 'Sales' },
  // { data: [28, 48, 40, 19, 86, 27, 90], label: 'Delivery' }

  public chartLabels: Array<any> = [];

  calendarMonths = [{name:'January'}, {name:'February'}, {name:'March'}, {name:'April'}, {name:'May'}, {name:'June'}, {name:'July'}, {name:'August'}, {name:'September'}, {name:'October'}, {name:'November'}, {name:'December'}]
  monthCustomer = []
  public chartColors: Array<any> = [
    {
      backgroundColor: '#00006387',
      borderColor: '#000063',
      borderWidth: 2,
    },
    {
      backgroundColor: 'rgba(0, 137, 132, .2)',
      borderColor: 'rgba(0, 10, 130, .7)',
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

  accountBalance = 0
  totalDebits =  0
  totalCredits = 0
  totalFees = 0
  totalPayfastCosts= 0
  currentBalance= 0

  //orders details
  ordersReceived = 0
  ordersCompleted = 0
  ordersInProgress = 0
  ordersThisMonth = 0
  ordersRevenue = 0.0
  ordersRevenueLastMonth = 0.0
  ordersRevenueCurrentMonth = 0.0

  //payout details
  serviceFees = 0
  paidOutRequests = 0
  processedRequests = 0
  newRequests = 0

  //courier details
  courierRevenue = 0
  courierRevenuelastMonth = 0
  courierRevenuethisMonth = 0
  courierRequests = 0

  pageReady = false
  apiCallsReady = 0

  ngOnInit() {

    this.getAccountBalances()
    this.getSalesDetails()
    this.getOrdersDetails()
    this.getPayoutDetails()
    this.getCourierDetails()
    this.getMonthlyCustomers()
  }

  getAccountBalances() {

    this._data.dynamicFormPost({}, 'admin-system-account-balances') 
      .pipe(first())
      .subscribe(

        data => {
          //console.log(data);
          //console.log(this.modelData)
          if(data['systemBalanceDetails'] != null) {

            if(data['systemBalanceDetails']['systemBalance'] != null) {
              this.accountBalance = data['systemBalanceDetails']['systemBalance']['currentSystemBalance']
              this.totalDebits = data['systemBalanceDetails']['systemBalance']['totalDebits']
              this.totalCredits = data['systemBalanceDetails']['systemBalance']['totalCredits']
              this.totalPayfastCosts = data['systemBalanceDetails']['systemBalance']['totalSystemPayfastCosts']
              this.currentBalance = data['systemBalanceDetails']['systemBalance']['currentSystemBalance']
            }
          }

          this.apiCallsReady++
        }, 

        error => {
          this.apiCallsReady++
        },

        () => {

        }
      );
  }

  getOrdersDetails() {

    let queryData = new FormData();
    queryData.append('modelName', 'BackOrder');

    this._data.getModelData(queryData) 
      .pipe(first())
      .subscribe(

        data => {
          if(data['covers'].length > 0) {

            this.ordersReceived = data['covers'].length
            let ordersCompleted = data['covers'].filter(order => order['cover']['model']['modelData']['systemManagedEntities']['orderDetails']['status'] == 'DELIVERED');
            this.ordersCompleted = ordersCompleted.length

            this.ordersInProgress = this.ordersReceived - ordersCompleted
            
          }

          this.apiCallsReady++
        }, 

        error => {
          this.apiCallsReady++
        },

        () => {

        }
      );
  }

  getSalesDetails() {

    let queryData = new FormData();
    queryData.append('modelName', 'Sale');

    this._data.getModelData(queryData) 
      .pipe(first())
      .subscribe(

        data => {
          if(data['covers'].length > 0) {

            for (let index = 0; index < data['covers'].length; index++) {
              const sale = data['covers'][index];

              this.ordersRevenue += parseFloat(sale['cover']['model']['modelData']['systemManagedEntities']['saleDetails']['totalAmount']);

              var dateCreated = new Date(sale['created'] * 1000);
              var saleyear = dateCreated.getFullYear();
              var salemonth = dateCreated.getMonth();


              var d = new Date();
              var year = d.getFullYear();
              var month = d.getMonth();

              

              if(saleyear == year && salemonth == month) {
                this.ordersRevenueCurrentMonth += parseFloat(sale['cover']['model']['modelData']['systemManagedEntities']['saleDetails']['totalAmount']);
              }

              d.setMonth(d.getMonth() - 1);
              var year = d.getFullYear();
              var month = d.getMonth();

              if(saleyear == year && salemonth == month) {
                this.ordersRevenueLastMonth += parseFloat(sale['cover']['model']['modelData']['systemManagedEntities']['saleDetails']['totalAmount']);
              }
              
            }
          }

          this.apiCallsReady++
        }, 

        error => {
          this.apiCallsReady++
        },

        () => {

        }
      );
  }

  getPayoutDetails() {

    let queryData = new FormData();
    queryData.append('modelName', 'CashOutTransaction');

    this._data.getModelData(queryData) 
      .pipe(first())
      .subscribe(

        data => {

          if(data['covers'].length > 0) {

            let completedRequests = data['covers'].filter(order => order['cover']['model']['modelData']['systemManagedEntities']['cashOutDetails']['bankReference'] != '');

            for (let index = 0; index < data['covers'].length; index++) {
                const payoutRequest = data['covers'][index];

                this.serviceFees += parseFloat(payoutRequest['cover']['model']['modelData']['systemManagedEntities']['cashOutDetails']['transactionCharge'])

                if(payoutRequest['cover']['model']['modelData']['systemManagedEntities']['cashOutDetails']['bankReference'] != '') {
                  //completed payouts
                  this.paidOutRequests += parseFloat(payoutRequest['cover']['model']['modelData']['systemManagedEntities']['cashOutDetails']['transactionAmount'])
                }

                if(payoutRequest['cover']['model']['modelData']['systemManagedEntities']['cashOutDetails']['paymentId'] != '' && payoutRequest['cover']['model']['modelData']['systemManagedEntities']['cashOutDetails']['bankReference'] == '') {
                  //completed payouts
                  this.processedRequests += parseFloat(payoutRequest['cover']['model']['modelData']['systemManagedEntities']['cashOutDetails']['transactionAmount'])
                }

                else if(payoutRequest['cover']['model']['modelData']['systemManagedEntities']['cashOutDetails']['paymentId'] == '') {
                  this.newRequests += parseFloat(payoutRequest['cover']['model']['modelData']['systemManagedEntities']['cashOutDetails']['transactionAmount'])
                }
              
            }

          }

          this.apiCallsReady++
        }, 

        error => {
          this.apiCallsReady++
        },

        () => {

        }
      );
  }

  getCourierDetails() {
    let queryData = new FormData();
    queryData.append('modelName', 'CourierWaybill');

    this._data.getModelData(queryData) 
      .pipe(first())
      .subscribe(

        data => {
          if(data['covers'].length > 0) {

            this.courierRequests = data['covers'].length

            for (let index = 0; index < data['covers'].length; index++) {
              const deliveryRequest = data['covers'][index];

              this.courierRevenue += parseFloat(deliveryRequest['cover']['model']['modelData']['systemManagedEntities']['waybillDetails']['quoteAmount']);

              var dateCreated = new Date(deliveryRequest['created'] * 1000);
              var saleyear = dateCreated.getFullYear();
              var salemonth = dateCreated.getMonth();


              var d = new Date();
              var year = d.getFullYear();
              var month = d.getMonth();

              

              if(saleyear == year && salemonth == month) {
                this.courierRevenuethisMonth += parseFloat(deliveryRequest['cover']['model']['modelData']['systemManagedEntities']['waybillDetails']['quoteAmount']);
              }

              d.setMonth(d.getMonth() - 1);
              var year = d.getFullYear();
              var month = d.getMonth();

              if(saleyear == year && salemonth == month) {
                this.courierRevenuelastMonth += parseFloat(deliveryRequest['cover']['model']['modelData']['systemManagedEntities']['waybillDetails']['quoteAmount']);
              }
              
            }
          }

          this.apiCallsReady++
        }, 

        error => {
          this.apiCallsReady++
        },

        () => {

        }
      );
  }

  getMonthlyCustomers() {

    let queryData = new FormData();
    queryData.append('modelName', 'CustomerProfile');

    this._data.getModelData(queryData) 
      .pipe(first())
      .subscribe(

        data => {

          var d = new Date();
          var year = d.getFullYear();
          var month = d.getMonth() + 1;

          let timestamp = this.toTimestamp(year, 0, 1)

          let newCustomers = data['covers'].filter(cust => cust['created'] > timestamp);
          console.log(newCustomers)


          //create chart labels
          for (let index = 0; index < month; index++) {

            const element = this.calendarMonths[index];
            this.chartLabels.push(this.calendarMonths[index]['name']);
            this.monthCustomer.push(0)

          }

          //create chart data
          for (let index = 0; index < newCustomers.length; index++) {
            const customer = newCustomers[index];

            var dateCreated = new Date(customer['created'] * 1000);
            var monthCreated = dateCreated.getMonth();

            this.monthCustomer[monthCreated] += 1
            
          }
          

          let newDataset = {
            data: this.monthCustomer,
            label: "New Customers"
          }

          this.chartDatasets.push(newDataset)

          console.log(newDataset)

          this.chartReady = true
          this.apiCallsReady++
          console.log(this.apiCallsReady)

        }, 

        error => {
          this.apiCallsReady++
        },

        () => {

        }
      );

  }

  toTimestamp(year,month,day){
    var datum = new Date(Date.UTC(year,month,day));
    return datum.getTime()/1000;
   }

}
