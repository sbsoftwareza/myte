import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-waybill-parcel-items',
  templateUrl: './waybill-parcel-items.component.html',
  styleUrls: ['./waybill-parcel-items.component.scss']
})
export class WaybillParcelItemsComponent implements OnInit {

  @Input() modelData;
  @Input() modelName;
  orderItems = []
  selected
  currentProduct
  opened = false
  modelConfiguration
  // modelName = "INBOUNDWAYBILL"

  footerActions = [
    {
      action: "update-status",
      title: "Mark As Received",
      options: [
        {label: "Parcel Received", val: "RECEIVED"},
        {label: "Parcel Rejected", val: "REJECTED"},
        {label: "Parcel Items Incomplete", val: "INCOMPLETE_ITEMS"},
      ],
      key : "parcelDetails->status"
    }
  ]

  constructor() { }

  ngOnInit() {

    console.log(this.modelData)
    this.modelConfiguration = environment.dashboardConfig.modelSettings[this.modelName];

    if(this.modelData.cover && this.modelData.cover.model && this.modelData.cover.model.modelData && this.modelData.cover.model.modelData.systemManagedEntities && this.modelData.cover.model.modelData.systemManagedEntities.waybillDetails) {
      this.orderItems = this.modelData.cover.model.modelData.systemManagedEntities.waybillDetails.items

      if(this.orderItems.length > 0) {
        this.selected = this.orderItems[0].name
        this.currentProduct = this.orderItems[0]
      }

      console.log(this.orderItems)
    }
    
  }

  viewProductImage() {

  }

  select(item, currentProduct) {
    this.selected = item;
    this.currentProduct = currentProduct
  }

  isActive(item) {
    return this.selected == item;
  }

  getSelected() {
    return this.selected
  }

}
