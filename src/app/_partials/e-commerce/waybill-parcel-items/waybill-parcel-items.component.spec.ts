import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaybillParcelItemsComponent } from './waybill-parcel-items.component';

describe('WaybillParcelItemsComponent', () => {
  let component: WaybillParcelItemsComponent;
  let fixture: ComponentFixture<WaybillParcelItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaybillParcelItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaybillParcelItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
