import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-view-order-items',
  templateUrl: './view-order-items.component.html',
  styleUrls: ['./view-order-items.component.scss']
})
export class ViewOrderItemsComponent implements OnInit {

  // @Input() orderItems;
  @Input() modelData;
  orderItems = []
  selected
  currentProduct
  opened = false

  constructor() { }

  ngOnInit() {

    console.log(this.modelData)

    if(this.modelData.cover && this.modelData.cover.model && this.modelData.cover.model.modelData && this.modelData.cover.model.modelData.systemManagedEntities && this.modelData.cover.model.modelData.systemManagedEntities.items) {
      this.orderItems = this.modelData.cover.model.modelData.systemManagedEntities.items

      if(this.orderItems.length > 0) {
        this.selected = this.orderItems[0].productName
        this.currentProduct = this.orderItems[0]
      }

      console.log(this.orderItems)
    }
    
  }

  viewProductImage() {

  }

  select(item, currentProduct) {
    this.selected = item;
    this.currentProduct = currentProduct
  }

  isActive(item) {
    return this.selected == item;
  }

  getSelected() {
    return this.selected
  }

}
