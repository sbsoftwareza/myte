import { Component, OnInit , Input, OnChanges, SimpleChanges} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { ExpressFunctionsService } from '../../../_services/express-functions.service';
import { UpdateModelStatusComponent } from 'src/app/_partials/system/dynamic-functions-modals/update-model-status/update-model-status.component';
import { FlagModelWithReasonComponent } from 'src/app/_partials/system/dynamic-functions-modals/flag-model-with-reason/flag-model-with-reason.component';
import { UnflagModelWithReasonComponent } from 'src/app/_partials/system/dynamic-functions-modals/unflag-model-with-reason/unflag-model-with-reason.component';

@Component({
  selector: 'app-dynamic-functions-footerbuttons',
  templateUrl: './dynamic-functions-footerbuttons.component.html',
  styleUrls: ['./dynamic-functions-footerbuttons.component.scss']
})
export class DynamicFunctionsFooterbuttonsComponent implements OnInit {

  @Input() modelInitData;
  @Input() modelName;
  @Input() modelData;
  @Input() action;

  config;
  modelDynamicActions = [];
  systemCustomeActions = [];

  constructor(private modalService: NgbModal, ) { }

  ngOnInit() {
    this.config = this.modelInitData;
  }

  ngOnChanges(changes: SimpleChanges) {
    // only run when property "data" changed
      if (changes['modelInitData']) {
      
          this.config = changes['modelInitData'].currentValue;
      }

      if (changes['modelName']) {
      
          this.modelName = changes['modelName'].currentValue;
      }

      if (changes['modelData']) {
      
          this.modelData = changes['modelData'].currentValue;
      }
  }

  dynamicActions(actiontype) {

    let data = this.action

    //all dynamic actions should be listed in the following case statement
    switch (actiontype) {

      case "update-status":

        const modalRef = this.modalService.open(UpdateModelStatusComponent);
        modalRef.componentInstance.data = {modelName: this.modelName, modelData: this.modelData, action: data};
        
        break;
    
      default:
        break;
    }
  }

}
