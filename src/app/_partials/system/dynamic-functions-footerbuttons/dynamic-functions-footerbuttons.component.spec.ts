import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicFunctionsFooterbuttonsComponent } from './dynamic-functions-footerbuttons.component';

describe('DynamicFunctionsFooterbuttonsComponent', () => {
  let component: DynamicFunctionsFooterbuttonsComponent;
  let fixture: ComponentFixture<DynamicFunctionsFooterbuttonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicFunctionsFooterbuttonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFunctionsFooterbuttonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
