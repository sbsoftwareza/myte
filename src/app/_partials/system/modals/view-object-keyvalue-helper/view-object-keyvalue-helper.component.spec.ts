import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewObjectKeyvalueHelperComponent } from './view-object-keyvalue-helper.component';

describe('ViewObjectKeyvalueHelperComponent', () => {
  let component: ViewObjectKeyvalueHelperComponent;
  let fixture: ComponentFixture<ViewObjectKeyvalueHelperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewObjectKeyvalueHelperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewObjectKeyvalueHelperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
