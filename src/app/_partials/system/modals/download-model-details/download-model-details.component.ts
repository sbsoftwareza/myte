import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-download-model-details',
  templateUrl: './download-model-details.component.html',
  styleUrls: ['./download-model-details.component.scss']
})
export class DownloadModelDetailsComponent implements OnInit {

  @Input() data;

  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit() {
  }

}
