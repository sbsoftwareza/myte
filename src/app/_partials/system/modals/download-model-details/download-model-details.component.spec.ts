import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadModelDetailsComponent } from './download-model-details.component';

describe('DownloadModelDetailsComponent', () => {
  let component: DownloadModelDetailsComponent;
  let fixture: ComponentFixture<DownloadModelDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadModelDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadModelDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
