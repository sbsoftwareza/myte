import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-email-model-details',
  templateUrl: './email-model-details.component.html',
  styleUrls: ['./email-model-details.component.scss']
})
export class EmailModelDetailsComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
