import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailModelDetailsComponent } from './email-model-details.component';

describe('EmailModelDetailsComponent', () => {
  let component: EmailModelDetailsComponent;
  let fixture: ComponentFixture<EmailModelDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailModelDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailModelDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
