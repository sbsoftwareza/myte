import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelListBreadcrumbsComponent } from './model-list-breadcrumbs.component';

describe('ModelListBreadcrumbsComponent', () => {
  let component: ModelListBreadcrumbsComponent;
  let fixture: ComponentFixture<ModelListBreadcrumbsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelListBreadcrumbsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelListBreadcrumbsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
