import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemHistorySummaryComponent } from './item-history-summary.component';

describe('ItemHistorySummaryComponent', () => {
  let component: ItemHistorySummaryComponent;
  let fixture: ComponentFixture<ItemHistorySummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemHistorySummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemHistorySummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
