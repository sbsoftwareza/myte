import { Component, OnInit , Input} from '@angular/core';
import { CommonDataService } from '../../../_services/common-data.service';
import { first } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-form-wizard',
  templateUrl: './form-wizard.component.html',
  styleUrls: ['./form-wizard.component.scss']
})
export class FormWizardComponent implements OnInit {

  @Input() modelName;

  modelData;
  config;
  accodionSettings;
  modelFormTemplate;
  postEndpoint  = "api-update-model-data";
  selected;
  selectedItemIndex;
  formData;
  Formsections;
  formId = 0;
  

  formReady = false;

  constructor(private _data: CommonDataService) { }

  ngOnInit() {
    this.config = environment.dashboardConfig.modelSettings[this.modelName];

    console.log(this.config)

    if(this.config['createModel'] === true) {
      this.Formsections = this.config['createModelSettings']['sections'];

      //console.log(this.Formsections)

      this.selected = this.config['createModelSettings']['sections'][0]['key'];
      this.selectedItemIndex = 0;
    }

    this.createModelForm();
    
  }

  createModelForm() {

    let queryData = new FormData();
    queryData.append('modelName', this.modelName);
    queryData.append('objectKeys', this.getSelected());
  
    this._data.createModelForm(queryData) 
    .pipe(first())
    .subscribe(
  
      data => {
        
        this.modelFormTemplate = data['definitionObjectForm'][this.modelName];
        this.formReady = true;
  
      },
  
      error => {
        console.log(error);
  
      },
  
      () => {
  
      }
    );
  }

  select(item, i) {
    this.selected = item;
    this.selectedItemIndex = i;
    this.createModelForm();
  }

  isActive(item) {
      return this.selected == item;
  }

  getSelected() {
    return this.selected
  }

  onFormSubmmited($event) {

    //this.formReady = false;

    let response = $event;

    //now we first check if we have the next section to submit
    this.formId = response['result']['id'];

    if(this.config['createModelSettings']['sections'].hasOwnProperty(this.selectedItemIndex + 1)) {  
      //we do have the next section
      let curremtIndex = this.selectedItemIndex + 1;
      let item = this.config['createModelSettings']['sections'][curremtIndex]['key'];
      this.select(item, curremtIndex);
 
    }

    this.formData = response['result']['model']['modelData']['formFieldsEntities'];
   // this.formReady = true;

    

    //this.formData = response['data'];

    //go to the next section if its there...

    //change the section key...then change the form id ...then change the form data
  }

}
