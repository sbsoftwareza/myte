import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelDataSummaryViewComponent } from './model-data-summary-view.component';

describe('ModelDataSummaryViewComponent', () => {
  let component: ModelDataSummaryViewComponent;
  let fixture: ComponentFixture<ModelDataSummaryViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelDataSummaryViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelDataSummaryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
