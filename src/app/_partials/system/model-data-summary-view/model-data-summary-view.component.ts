import { Component, OnInit , Input, OnChanges, SimpleChanges} from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-model-data-summary-view',
  templateUrl: './model-data-summary-view.component.html',
  styleUrls: ['./model-data-summary-view.component.scss']
})
export class ModelDataSummaryViewComponent implements OnInit , OnChanges {

  @Input() modelDetailsName;
  @Input() modelId;
  @Input() modelInitData;

  modelData;
  config;
  modelName;
  modelConfiguration

  constructor() { }

  ngOnInit() {

    this.modelName = this.modelDetailsName;
    this.config = environment.dashboardConfig.modelSettings[this.modelName].partialInfoSetup;
    this.modelConfiguration = environment.dashboardConfig.modelSettings[this.modelName];
    this.modelData = this.modelInitData;
  }

  getModelValueFromMappingKey( valueMappingKey) {

    //systemManagedEntities->paymentDetails->total

    var model = this.modelData;

    if(valueMappingKey == 'id') {
      return String(model['id']);
    }

    if(valueMappingKey == 'created') {

      var currentDate = new Date(model['created']*1000);

      var date = currentDate.getDate();
      var month = currentDate.getMonth(); //Be careful! January is 0 not 1
      var year = currentDate.getFullYear();

      var dateString = date + "-" +(month + 1) + "-" + year;
      return String(dateString);
    }

    if(valueMappingKey == 'last_updated') {
      var currentDate = new Date(model['last_updated']*1000);

      var date = currentDate.getDate();
      var month = currentDate.getMonth(); //Be careful! January is 0 not 1
      var year = currentDate.getFullYear();

      var dateString = date + "-" +(month + 1) + "-" + year;
      return String(dateString);
    }


    let objectPath = valueMappingKey.split('->');

    var objectName, sectionName, valueKey, value;

    if(objectPath[0]  && objectPath[1] ) {

      // value = model['cover']['model']['modelData'][objectPath[0]][objectPath[1]];

      // if(objectPath[2]) {
      //   value = value[objectPath[2]];
      // }

      value = '';

      if(model['cover']['model']['modelData'].hasOwnProperty(objectPath[0]) ) {

        if(model['cover']['model']['modelData'][objectPath[0]].hasOwnProperty(objectPath[1])) {
            value = model['cover']['model']['modelData'][objectPath[0]][objectPath[1]];

            // console.log(model);
      
            if(objectPath[2]) {
              value = value[objectPath[2]];
            }
        }

      }

    }
    
    if(value === '')
      value = 'N/A';

    return value;

  }

  ngOnChanges(changes: SimpleChanges) {
      // only run when property "data" changed
      if (changes['modelInitData']) {
      
          this.modelData = changes['modelInitData'].currentValue;
      }
  }

}
