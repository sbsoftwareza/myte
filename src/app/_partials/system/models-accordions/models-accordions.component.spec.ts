import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelsAccordionsComponent } from './models-accordions.component';

describe('ModelsAccordionsComponent', () => {
  let component: ModelsAccordionsComponent;
  let fixture: ComponentFixture<ModelsAccordionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelsAccordionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelsAccordionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
