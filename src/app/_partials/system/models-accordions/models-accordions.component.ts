import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from '../../../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonDataService } from '../../../_services/common-data.service';
import { first } from 'rxjs/operators';

import { DynamicFormModalComponent } from 'src/app/_partials/system/modals/dynamic-form-modal/dynamic-form-modal.component';
import { splitAtColon } from '@angular/compiler/src/util';

@Component({
  selector: 'app-models-accordions',
  templateUrl: './models-accordions.component.html',
  styleUrls: ['./models-accordions.component.scss']
})
export class ModelsAccordionsComponent implements OnInit , OnChanges {
  selected: any;
  closeResult: string;

  endpoint = "api-update-model-data";

  @Input() modelDetailsName;
  @Input() modelId;
  @Input() modelInitData;
  modelData;
  config;
  accodionSettings;
  modelName;
  modelFormTemplate;
  UseSectionkeyValues = false;

  modelCount;
  modelQueryId;
  files = false;

  dataReady =false;
  // opened = true

  constructor(private _data: CommonDataService, private modalService: NgbModal) { }

  ngOnInit() {
    // console.log(this.modelConfig)
    // this.config = this.modelConfig;

    this.modelName = this.modelDetailsName;
    this.modelQueryId = this.modelId;

    this.config = environment.dashboardConfig.modelSettings[this.modelName].partialInfoSetup;
    this.accodionSettings = environment.dashboardConfig.modelSettings[this.modelName].accodionDisplayInfo;

    if(environment.dashboardConfig.modelSettings[this.modelName].files) {
      this.files = environment.dashboardConfig.modelSettings[this.modelName].files;
    }
      

    var data;

    this.selected = this.accodionSettings.sections[0].section;

    this.createModelForm(this.config);

    if(this.modelInitData && this.modelInitData.cover) {
      
      this.modelData = this.modelInitData;
    }

    else
      data = this.queryModelFromApi(this.config);

    
}

createModelForm (config) {

  let queryData = new FormData();
  queryData.append('modelName', config.name);
  queryData.append('objectKeys', this.getSelected());

  this._data.createModelForm(queryData) 
  .pipe(first())
  .subscribe(

    data => {
      
      if(data['definitionObjectForm'][config.name]['sections'] != undefined && typeof(data['definitionObjectForm'][config.name]['sections']) == 'object' && Object.keys(data['definitionObjectForm'][config.name]['sections']).length > 0) {
        this.modelFormTemplate = data['definitionObjectForm'][config.name];
        this.UseSectionkeyValues = true;
      }

      else {
        this.UseSectionkeyValues = false;
      }

      // this.modelFormTemplate = data['definitionObjectForm'][config.name];

    },

    error => {
      console.log(error);

    },

    () => {

    }
  );
}

queryModelFromApi(model) {

  let queryData = new FormData();
  queryData.append('modelName', model.name);

  if(parseInt(this.modelQueryId) > 0) {
    queryData.append('mid', this.modelQueryId);
  }

  this._data.getModelData(queryData) 
  .pipe(first())
  .subscribe(

    data => {
      
      
      this.modelCount = Object.keys(data['covers']).length;
      this.modelData = data['covers'][this.modelCount - 1];

      this.dataReady = true;

      //console.log(this.modelData);

    },

    error => {
      console.log(error);
      //   iziToast.success({
      //     title: 'Error!',
      //     position: 'topRight',
      //     icon: 'icon-circle-check',
      //     message: 'Failed to retrieve profile info.'
      // });

    },

    () => {
        return this.modelData;
    }
  );

}

  select(item) {
    this.selected = item;
    this.createModelForm(this.config);
  }

  isActive(item) {
      return this.selected == item;
  }

  getSelected() {
    return this.selected
  }

  isExcluded(field, i) {
    
    if(this.accodionSettings.sections[i].excludes != undefined) {
      let excludes = this.accodionSettings.sections[i].excludes.split(",")

      // return excludes.includes(field);
      
    }
    return false;
  }

  getModelValueFromMappingKey( valueMappingKey) {

    //systemManagedEntities->paymentDetails->total

    var model = this.modelData;


    let objectPath = valueMappingKey.split('->');

    var objectName, sectionName, valueKey;
    var value = '';

    if(objectPath[0]  && objectPath[1] ) {

      let section = model['cover']['model']['modelData'][objectPath[0]][objectPath[1]];

      // console.log(model);

      if(objectPath[2] && section[objectPath[2]]) {
        value = section[objectPath[2]];
      }
        
        //console.log(value);

    }
    
    if(value === '')
      value = 'N/A';

    return value;

  }

  toArrayObjects(obj) {
    return Object.keys(obj);
 }

 editSelectedSectionInFormModal () {
   
  this.createModelForm(this.config);
  const modalRef = this.modalService.open(DynamicFormModalComponent);

  modalRef.componentInstance.data = this.modelData['cover']['model']['modelData']['formFieldsEntities'];
  modalRef.componentInstance.form = this.modelFormTemplate;
  modalRef.componentInstance.postEndpoint = this.endpoint;
  modalRef.componentInstance.formId = this.modelQueryId;

}

ngOnChanges(changes: SimpleChanges) {
  // only run when property "data" changed
  if (changes['modelInitData']) {
     // alert('changed')
      this.modelData = changes['modelInitData'].currentValue;
  }
}

}
