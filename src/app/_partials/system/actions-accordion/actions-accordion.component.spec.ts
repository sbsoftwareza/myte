import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionsAccordionComponent } from './actions-accordion.component';

describe('ActionsAccordionComponent', () => {
  let component: ActionsAccordionComponent;
  let fixture: ComponentFixture<ActionsAccordionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionsAccordionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionsAccordionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
