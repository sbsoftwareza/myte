import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EmailSuccessComponent } from 'src/app/_partials/system/modals/email-success/email-success.component';
import { DownloadModelDetailsComponent } from 'src/app/_partials/system/modals/download-model-details/download-model-details.component';
import { DynamicFormModalComponent } from 'src/app/_partials/system/modals/dynamic-form-modal/dynamic-form-modal.component';

import { AuthService } from '../../../_services/auth.service';
import { environment } from '../../../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-actions-accordion',
  templateUrl: './actions-accordion.component.html',
  styleUrls: ['./actions-accordion.component.scss']
})
export class ActionsAccordionComponent implements OnInit {

  public isCollapsed = true;
  public isCollapsedMessages = true;
  public isCollapsedProfile = true;
  
  greeting;
  userDetails;
  name = "";
  actionsConfig = environment.dashboardConfig.landingPageConfig.actions;

  constructor(private modalService: NgbModal, private authService: AuthService, private router: Router) { }

  ngOnInit() {

    this.getTimeOfDay();
    this.userDetails = this.authService.getSessionDetails('PersonalDetails');

    if(this.userDetails.first_name) {
      this.name = this.userDetails.first_name;
    }
    
  }

  actionBoxAction(item : any) {
      if(item.modal) {
          switch(item.modalName) {

            case "email": {
                this.openEmailModal();
                break; 
            }

            case "download": {
                this.openDownloadModal();
                break; 
            }

            case "form": {
                this.openFormModal();  
                break;            
            }

            default: { 
              //statements; 
              break; 
            } 

          }
      }

      else {
        this.router.navigate([item.linkUrl]);
      }
  }

  //Report Action Modals

  openEmailModal() {
    const modalRef = this.modalService.open(EmailSuccessComponent);
    modalRef.componentInstance.data = {email: "brian@hellocavalry.com"};
  }

  openDownloadModal() {
    const modalRef = this.modalService.open(DownloadModelDetailsComponent);
    modalRef.componentInstance.data = {email: "brian@hellocavalry.com"};
  }

  openFormModal() {
    const modalRef = this.modalService.open(DynamicFormModalComponent);
    modalRef.componentInstance.data = {email: "brian@hellocavalry.com"};
  }

  getTimeOfDay() {
    var now  = new Date(),
    hour = now.getHours();

    var shift   = "Good ";
        shift  += (hour >= 4  && hour <= 11) ? "morning" : "",
        shift  += (hour >= 12 && hour <= 16) ? "afternoon" : "",
        shift  += (hour >= 17 && hour <= 20) ? "evening" : "",
        shift  += (hour >= 21 || hour <= 3) ?  "evening" : "";

    this.greeting = shift;
  }

}
