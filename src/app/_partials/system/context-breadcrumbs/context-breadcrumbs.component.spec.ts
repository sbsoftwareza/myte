import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContextBreadcrumbsComponent } from './context-breadcrumbs.component';

describe('ContextBreadcrumbsComponent', () => {
  let component: ContextBreadcrumbsComponent;
  let fixture: ComponentFixture<ContextBreadcrumbsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContextBreadcrumbsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextBreadcrumbsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
