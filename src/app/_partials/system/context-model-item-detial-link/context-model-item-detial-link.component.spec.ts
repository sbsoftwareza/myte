import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContextModelItemDetialLinkComponent } from './context-model-item-detial-link.component';

describe('ContextModelItemDetialLinkComponent', () => {
  let component: ContextModelItemDetialLinkComponent;
  let fixture: ComponentFixture<ContextModelItemDetialLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContextModelItemDetialLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextModelItemDetialLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
