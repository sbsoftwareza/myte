import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicFunctionsDropdownhandlerComponent } from './dynamic-functions-dropdownhandler.component';

describe('DynamicFunctionsDropdownhandlerComponent', () => {
  let component: DynamicFunctionsDropdownhandlerComponent;
  let fixture: ComponentFixture<DynamicFunctionsDropdownhandlerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicFunctionsDropdownhandlerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFunctionsDropdownhandlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
