import { Component, OnInit , Input, OnChanges, SimpleChanges} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { ExpressFunctionsService } from '../../../_services/express-functions.service';
import { UpdateModelStatusComponent } from 'src/app/_partials/system/dynamic-functions-modals/update-model-status/update-model-status.component';
import { FlagModelWithReasonComponent } from 'src/app/_partials/system/dynamic-functions-modals/flag-model-with-reason/flag-model-with-reason.component';
import { UnflagModelWithReasonComponent } from 'src/app/_partials/system/dynamic-functions-modals/unflag-model-with-reason/unflag-model-with-reason.component';
import { PayoutProofOfPaymentComponent } from 'src/app/_partials/e-commerce/modals/payout-proof-of-payment/payout-proof-of-payment.component';

import { Router, ActivatedRoute } from '@angular/router';
import { CommonDataService } from '../../../_services/common-data.service';
import { environment } from '../../../../environments/environment';


@Component({
  selector: 'app-dynamic-functions-dropdownhandler',
  templateUrl: './dynamic-functions-dropdownhandler.component.html',
  styleUrls: ['./dynamic-functions-dropdownhandler.component.scss']
})
export class DynamicFunctionsDropdownhandlerComponent implements OnInit , OnChanges {

  @Input() modelInitData;
  @Input() modelName;
  @Input() modelData;

  config;
  modelDynamicActions = [];
  systemCustomeActions = [];

  constructor(private modalService: NgbModal, private _data: CommonDataService, private router: Router ) { }

  ngOnInit() {
    this.config = this.modelInitData;

    if(this.config.dynamicFunctions != undefined)
      this.modelDynamicActions = this.config.dynamicFunctions;
  }

  ngOnChanges(changes: SimpleChanges) {
    // only run when property "data" changed
      if (changes['modelInitData']) {
      
          this.config = changes['modelInitData'].currentValue;
      }

      if (changes['modelName']) {
      
          this.modelName = changes['modelName'].currentValue;
      }

      if (changes['modelData']) {
      
          this.modelData = changes['modelData'].currentValue;
      }
  }

  dynamicActions(action, data) {

    //all dynamic actions should be listed in the following case statement
    switch (action) {

      case "update-status":

        const modalRef = this.modalService.open(UpdateModelStatusComponent);
        modalRef.componentInstance.data = {modelName: this.modelName, modelData: this.modelData, action: data};
        
        break;

      case "confirm-payout-payment":

        const modalPaymentProof = this.modalService.open(PayoutProofOfPaymentComponent);
        modalPaymentProof.componentInstance.data = {modelName: this.modelName, modelData: this.modelData, action: data};
        
        break;

      case "view-context-model":

      console.log(this.modelData)

        let queryExtras =  [
          {
            key : 'context_id' , 
            value : this.modelData['context_id']
          }
        ];

        this.queryParamsSet(queryExtras, data)

        
        
        break;


        case "view-resource-link":

          let apikey = localStorage.getItem('apikey');
          let authkey = localStorage.getItem('authkey');

          const form = window.document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", environment.apiUrl + data['button']['resorce_link']);
            //use _self to redirect in same tab, _blank to open in new tab
            form.setAttribute("target", "_blank"); 
            // form.setAttribute("target", "_self"); 
      
            form.appendChild(this.createHiddenElement('modelName', this.modelName));
            form.appendChild(this.createHiddenElement('mid', this.modelData['id']));
            form.appendChild(this.createHiddenElement('apikey', apikey));
            form.appendChild(this.createHiddenElement('authkey', authkey));
            window.document.body.appendChild(form);

            console.log(form.elements)
      
            //console.log(form);
            form.submit();

        break;
    
      default:
        break;
    }
  }

  private createHiddenElement(name: string, value: string): HTMLInputElement {
    const hiddenField = document.createElement('input');
    hiddenField.setAttribute('name', name);
    hiddenField.setAttribute('value', value);
    hiddenField.setAttribute('type', 'hidden');
    return hiddenField;
  }

  queryParamsSet(dataParams: any, data) {
    this._data.queryParamsChanged.next(dataParams);
    this.router.navigate(['dashboard/model-list/'+data.modelName]);
  }

}
