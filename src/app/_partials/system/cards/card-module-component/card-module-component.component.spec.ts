import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardModuleComponentComponent } from './card-module-component.component';

describe('CardModuleComponentComponent', () => {
  let component: CardModuleComponentComponent;
  let fixture: ComponentFixture<CardModuleComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardModuleComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardModuleComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
