import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dynamic-menu-slider-card',
  templateUrl: './dynamic-menu-slider-card.component.html',
  styleUrls: ['./dynamic-menu-slider-card.component.scss']
})
export class DynamicMenuSliderCardComponent implements OnInit {

  @Input() cardMenus;

  constructor() { }

  ngOnInit() {
  }

}
