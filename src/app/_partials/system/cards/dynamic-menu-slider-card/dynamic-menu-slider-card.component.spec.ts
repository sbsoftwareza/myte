import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicMenuSliderCardComponent } from './dynamic-menu-slider-card.component';

describe('DynamicMenuSliderCardComponent', () => {
  let component: DynamicMenuSliderCardComponent;
  let fixture: ComponentFixture<DynamicMenuSliderCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicMenuSliderCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicMenuSliderCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
