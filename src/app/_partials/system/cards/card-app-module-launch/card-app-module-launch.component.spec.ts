import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardAppModuleLaunchComponent } from './card-app-module-launch.component';

describe('CardAppModuleLaunchComponent', () => {
  let component: CardAppModuleLaunchComponent;
  let fixture: ComponentFixture<CardAppModuleLaunchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardAppModuleLaunchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardAppModuleLaunchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
