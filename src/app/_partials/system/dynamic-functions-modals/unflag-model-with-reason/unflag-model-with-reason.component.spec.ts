import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnflagModelWithReasonComponent } from './unflag-model-with-reason.component';

describe('UnflagModelWithReasonComponent', () => {
  let component: UnflagModelWithReasonComponent;
  let fixture: ComponentFixture<UnflagModelWithReasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnflagModelWithReasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnflagModelWithReasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
