import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateModelStatusComponent } from './update-model-status.component';

describe('UpdateModelStatusComponent', () => {
  let component: UpdateModelStatusComponent;
  let fixture: ComponentFixture<UpdateModelStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateModelStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateModelStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
