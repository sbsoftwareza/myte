import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { CommonDataService } from '../../../../_services/common-data.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-update-model-status',
  templateUrl: './update-model-status.component.html',
  styleUrls: ['./update-model-status.component.scss']
})
export class UpdateModelStatusComponent implements OnInit {

  @Input() data;
  modelForm: FormGroup;

  submitButtonText = "Update Status"

  constructor(public activeModal: NgbActiveModal, private formBuilder: FormBuilder, private _data: CommonDataService) { }

  ngOnInit() {

    this.modelForm = this.formBuilder.group({

      status: ['', Validators.required], //Product name

    });
  }

  prepopulateCurrentStatus() {
    //day 2
  }

  updateStatus() {


    this.submitButtonText= 'Please Wait...';

    if (this.modelForm.invalid) {
      
      this.submitButtonText= 'Update Status';

      return;
    }

    let keyString = this.data.action.key
    let keys = keyString.split('->')
    let entityKey = ""
    let endpoint = "admin-update-model"
    // let entityField = ""

    if(keys[0] != undefined) 
      entityKey += keys[0]

    if(keys[1] != undefined) 
      entityKey += '-'+keys[1]

    if(keys[2] != undefined) 
      entityKey += '-'+keys[2]

    let modelObject1 = new FormData();

    modelObject1.append('modelName', this.data.modelName);
    modelObject1.append('id', this.data.modelData['id']);
    modelObject1.append(entityKey, this.modelForm.controls['status'].value);


    if(this.data.action.endpoint != undefined) {
      endpoint = this.data.action.endpoint 
    }

    this._data.dynamicFormPost(modelObject1, endpoint)
    .pipe(first())
    .subscribe(
  
      data => {
        this.submitButtonText= 'Update Status';
  
      },
  
      error => {
        this.submitButtonText= 'Update Status';
      },
  
      () => {
      }
    );
  }

}
