import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlagModelWithReasonComponent } from './flag-model-with-reason.component';

describe('FlagModelWithReasonComponent', () => {
  let component: FlagModelWithReasonComponent;
  let fixture: ComponentFixture<FlagModelWithReasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlagModelWithReasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlagModelWithReasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
