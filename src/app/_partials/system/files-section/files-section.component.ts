import { Component, OnInit, Input , OnChanges, SimpleChanges} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { environment } from '../../../../environments/environment';


@Component({
  selector: 'app-files-section',
  templateUrl: './files-section.component.html',
  styleUrls: ['./files-section.component.scss']
})
export class FilesSectionComponent implements OnInit , OnChanges {

  @Input() files;
  @Input() title;

  fileData = [];
  displayFiles = false;

  fileType = 'url'; //base64

  constructor(private _sanitizer: DomSanitizer) { }

  ngOnInit() {

    //this.fileData = this.files;

    if(typeof(this.files) === 'object' && this.fileType == 'base64') {
      var size = Object.keys(this.files).length;

      if(size > 0) {
        this.displayFiles = true;
        this.fileData = this.files;
      }
    }

    if(this.fileType == 'url') {
      if( this.files['files'] && this.files['files']['uploads'] && Array.isArray(this.files['files']['uploads']) && this.files['files']['uploads'].length > 0) {
        // let productImages = this.files['files']['uploads']
        this.fileData = this.files['files']['uploads'];
      }

      if( this.files['files'] && this.files['files']['images'] && Array.isArray(this.files['files']['images']) && this.files['files']['images'].length > 0) {
        // let productImages = this.files['files']['uploads']
        this.fileData = this.files['files']['images'];
      }
    }

  }

  ngOnChanges(changes: SimpleChanges) {
    // only run when property "data" changed
    if (changes['files'] && typeof(changes['files'].currentValue) === 'object') {
    
        
        //console.log(this.fileData);

        var size = Object.keys(changes['files'].currentValue).length;

        if(size > 0) {
          this.displayFiles = true;
          this.fileData = changes['files'].currentValue;
        }
    }
  }

  getImageSrc(file) {
    
    // data:{{file.type}};{{file.contentType}}
    let content = btoa(file.content)
    let filePath = this._sanitizer.bypassSecurityTrustResourceUrl('data:'+file.type+';'+file.contentType+',' 
                 + content);
    return filePath;
  }

  getProductImageSrc(element){

      if(element['tmp_name'] != undefined && element['tmp_name'] != '') {
        let srcStrings = element['tmp_name'].split("public/");
        let src = environment.filesSrc+'/'+srcStrings[1]
        element['src'] = src
        return src
      }

      return ''
  }

  

}
