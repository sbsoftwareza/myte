import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourierParcelItemsComponent } from './courier-parcel-items.component';

describe('CourierParcelItemsComponent', () => {
  let component: CourierParcelItemsComponent;
  let fixture: ComponentFixture<CourierParcelItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourierParcelItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourierParcelItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
