import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-courier-parcel-items',
  templateUrl: './courier-parcel-items.component.html',
  styleUrls: ['./courier-parcel-items.component.scss']
})
export class CourierParcelItemsComponent implements OnInit {

  @Input() modelData;
  @Input() modelName;
  orderItems = []
  selected
  currentProduct
  opened = false
  modelConfiguration

  footerActions = {
    "CourierWaybill" : {
      action: "assign-trip",
      title: "Assign To Nearby Driver",
      endpoint: "auto-assign-trip"
    }
  }

  constructor() { }

  ngOnInit() {

    console.log(this.modelData)
    this.modelConfiguration = environment.dashboardConfig.modelSettings[this.modelName];

    if(this.modelData.cover && this.modelData.cover.model && this.modelData.cover.model.modelData && this.modelData.cover.model.modelData.systemManagedEntities && this.modelData.cover.model.modelData.systemManagedEntities.parcelDetails) {
      this.orderItems = this.modelData.cover.model.modelData.systemManagedEntities.parcelDetails.items

      if(this.orderItems.length > 0) {
        this.selected = this.orderItems[0].name
        this.currentProduct = this.orderItems[0]
      }

      console.log(this.orderItems)
    }
    
  }

  viewProductImage() {

  }

  select(item, currentProduct) {
    this.selected = item;
    this.currentProduct = currentProduct
  }

  isActive(item) {
    return this.selected == item;
  }

  getSelected() {
    return this.selected
  }

}
