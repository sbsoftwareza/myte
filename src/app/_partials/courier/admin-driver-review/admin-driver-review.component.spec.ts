import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDriverReviewComponent } from './admin-driver-review.component';

describe('AdminDriverReviewComponent', () => {
  let component: AdminDriverReviewComponent;
  let fixture: ComponentFixture<AdminDriverReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDriverReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDriverReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
