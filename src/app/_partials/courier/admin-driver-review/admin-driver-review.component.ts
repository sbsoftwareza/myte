import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { CommonDataService } from '../../../_services/common-data.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-admin-driver-review',
  templateUrl: './admin-driver-review.component.html',
  styleUrls: ['./admin-driver-review.component.scss']
})
export class AdminDriverReviewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
