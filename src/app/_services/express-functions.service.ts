import { Injectable } from '@angular/core';
import { CommonDataService } from '../_services/common-data.service';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { switchMap, map, first } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { id } from '@swimlane/ngx-datatable';


@Injectable({
  providedIn: 'root'
})
export class ExpressFunctionsService {

  constructor(private _data: CommonDataService) { }

  dynamicFunctionSelector(funcName, data) {

  }

  async flagModel(id, modelName, reason) {
    let data = {
      productId: id,
      modelName: modelName,
      reason: reason
    }
    // let resp = await this.http.post<any>(`${environment.apiUrl}/`+endpoint, data).toPromise()
    let resp = await this._data.dynamicFormPost(data, 'flag-model').toPromise()
  }

  async unFlagModel(id, modelName, reason) {
    let data = {
      productId: id,
      modelName: modelName,
      reason: reason
    }
    let resp = await this._data.dynamicFormPost(data, 'unflag-model').toPromise()
  }

  async updateModelStatus(id, keyName, status) {
    let data = {
      productId: id,
      keyName: keyName,
      status: status
    }
    let resp = await this._data.dynamicFormPost(data, 'status-update').toPromise()
  }


}
