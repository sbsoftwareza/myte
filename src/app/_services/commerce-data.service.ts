import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { switchMap, map, first } from 'rxjs/operators';
import { forEach } from '@angular/router/src/utils/collection';
import { Subject } from 'rxjs';

import { CommonDataService } from './common-data.service';
import { AuthService } from './auth.service';
import { toASCII } from 'punycode';
import { stringify } from 'querystring';



const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Accept': 'application/json',
  }),
  withCredentials: false
};

@Injectable({
  providedIn: 'root'
})
export class CommerceDataService {

  private modelData = new BehaviorSubject<any>(null); // store for quote data
  public modelData$ = this.modelData.asObservable();
  subscription:any;


  cartItemsChanged = new Subject<any>();

  
  constructor(private http: HttpClient, private _data: CommonDataService,private authService: AuthService) { 
    this.subscription = this.cartItemsChanged.subscribe(
      (data) => {
        if(data && data.length > 0) {
          localStorage.setItem('cartItems', JSON.stringify(data));
        }

        else {
          localStorage.removeItem('cartItems');
        }
      }
    );
  }

  // General Data
  addToShoppingCart(product, quantity) {
    let items = []
    let newProductInCart = true

    if(this.checkProductStockQuantity(product['id'], product['variationId'], quantity)) {

      if (localStorage.getItem("cartItems") != null) {
        //...
        items = JSON.parse(localStorage.getItem("cartItems"))
  
        for (let index = 0; index < items.length; index++) {
          let cartItem = items[index];
          
          product['quantity'] = quantity
  
          if(cartItem['id'] == product['id'] ) {
            items[index]['quantity'] = parseInt(quantity) + parseInt(items[index]['quantity']);
            newProductInCart = false
          }
  
        }

        if(newProductInCart) {
          items.push(product)
        }
      }
  
      else {
  
        product['quantity'] = quantity
        items.push(product)
      }
  
      localStorage.setItem('cartItems', JSON.stringify(items));

      this.cartItemsChanged.next(items);


      this.addProductVirtualStock(product['id'], product['variationId'], quantity)

      return true //product added to shopping cart
    }

    else {
      return false //product out of stock and cannot be processed at the time
    }

  }

  emptyShoppingCart() {
    this.cartItemsChanged.next([]);
  }

  getCartEarnPoints() {
    return 0
  }

  updateShoppingCart(product, quantity) {
    let items = []
    let newProductInCart = true

    if(this.checkProductStockQuantity(product['id'], product['variationId'], quantity)) {

      if (localStorage.getItem("cartItems") != null) {
        //...
        items = JSON.parse(localStorage.getItem("cartItems"))
  
        for (let index = 0; index < items.length; index++) {
          let cartItem = items[index];
          
          product['quantity'] = quantity
  
          if(cartItem['id'] == product['id']) {
            // items[index]['quantity'] = quantity;
            items[index]['quantity'] = quantity;
            newProductInCart = false
          }
  
        }

        if(newProductInCart) {
          items.push(product)
        }
      }
  
      else {
  
        product['quantity'] = quantity
        items.push(product)
      }
  
      localStorage.setItem('cartItems', JSON.stringify(items));

      this.cartItemsChanged.next(items);


      this.addProductVirtualStock(product['id'], product['variationId'], quantity)

      return true //product added to shopping cart
    }

    else {
      return false //product out of stock and cannot be processed at the time
    }

  }

  calculateCartTotal() {

    let cartTotalBreakDown = {
      total : 0,
      vat: 0,
      shipping: 0,
      totalInclusive : 0,
      transactionFee: 0
    }

    let items = localStorage.getItem("cartItems") 

    if(items) {
      items = JSON.parse(localStorage.getItem("cartItems"))
      for (let index = 0; index < items.length; index++) {
        const element = items[index];
  
        console.log(element)
        let priceTotal = 0
        let vat = 0
        let total = 0
        let productTransactionFee = 0
        //check if the product is on sale and use sale price... if not use normal price
        if(element['cover']['model']['modelData']['formFieldsEntities']['productDetails']['on_sale'] == true) {
          let priceItem = parseFloat(element['cover']['model']['modelData']['formFieldsEntities']['productDetails']['sale_price'])
          priceTotal = priceItem * parseInt(element['quantity']) 
  
          vat = this.calculatetaxBracket(element['cover']['model']['modelData']['formFieldsEntities']['productDetails']['tax'], priceTotal)
  
        }
  
        else {
          let priceItem = parseFloat(element['cover']['model']['modelData']['formFieldsEntities']['productDetails']['regular_price'])
          priceTotal = priceItem * parseInt(element['quantity']) 
  
          vat = this.calculatetaxBracket(element['cover']['model']['modelData']['formFieldsEntities']['productDetails']['tax'], priceTotal)
  
        }
  
        total = vat + priceTotal + productTransactionFee
  
        cartTotalBreakDown.total += priceTotal
        cartTotalBreakDown.vat += vat
        cartTotalBreakDown.transactionFee += productTransactionFee
        cartTotalBreakDown.totalInclusive += total
        
      }
    }

    return cartTotalBreakDown

  }

  checkProductStockQuantity(productId, variationId, quantity) {
    //if product is smyte warehoused 
        //check stock from the warehouse stock 
        //then further deduct any stock in virtual stock table 
    //if stock is seller managed, check stock from the product quantity, then further deduct and stock in virtual stock

    return true
  }

  addProductVirtualStock(productId, variationId, quantity) {
    //when a product is added to shopping cart it is added to virtual stock
    //this should expire after an hour if not purchased
    //virtual stock will be a redis database used to track shopping basket orders

  }

  calculateOrderSummary() {

  }



  getCourierCost(products, shippingMethodId , type) {
     let cartdata = {
       methodId: shippingMethodId,
       type: type,
       items: products
     }

     this._data.dynamicFormPost(cartdata, 'courier-get-quote') 
        .pipe(first())
        .subscribe(
          data => {
            console.log(data);

            // alert(data['quote']['methodId']['fee']);
            alert('test complete')

          },
          error => {
    
          },

          () => {

          }
      );
  }

  resetShopCartShippingCosts() {
    sessionStorage.removeItem('shippingCost')
  }

  createCourierRequest(cost, methodName, methodId, orderid ) {
    //this creates a courier delivery request 
    //with order id, 
    //method name
    //cost 

    //back end will take the request and create a courier request, confirm the totalcost of shipping
    //with delivery address
    //with product IDs
    //each product pickup location and dropoff location 

    //when payment is fullfilled
    //backend will create all the waybill requests (INBOUND : for products coming in for packaging and OUTBOUND for final delivery)
  }

  removeFromShoppingCart(productId) {

    if (localStorage.getItem("cartItems")) {
      //...
      let items = JSON.parse(localStorage.getItem("cartItems"))

      for (let index = 0; index < items.length; index++) {
        let cartItem = items[index];
        
        if(cartItem['id'] == productId ) {
          items.splice(index, 1)
        }

      }

      if(items.length > 0) {
        localStorage.setItem('cartItems', JSON.stringify(items));

        this.cartItemsChanged.next(items);
      }

      else {
        this.cartItemsChanged.next([]);
      }

      

    }
  }

  checkInstantAddToCart(productModelData) {

    if(productModelData['cover']['model']['modelData']['formFieldsEntities']['productDetails']['sizevariations'] ) {
        let variations = JSON.parse(productModelData['cover']['model']['modelData']['formFieldsEntities']['productDetails']['sizevariations'])

        if(variations.length > 1) {
          //we have variations, so no to instant cart add
          return false
        }

        else  
          return true
        
    }

    else {
      return true
    }

  }

  sortProducts(products) {
    // descending
    let newarr = products.sort((a, b) => b.index - a.index);

    //ascending
    let newarra = products.sort((a, b) => a.index - b.index);
  }

  getUserWishlistAndAppendNewItem(productId) {
    //check if user is signed in,
    let userId = sessionStorage.getItem('context')
    if(this.authService.isUserLoggedIn() && userId) {
      //if yes get a wishlist from the API 
      let modelObject1 = new FormData();

      modelObject1.append('modelName', 'WishList');
      modelObject1.append('context_id', userId);

    //lets get the model for this page
        this._data.getModelData(modelObject1) 
        .pipe(first())
        .subscribe(
          data => {
            console.log(data);
            if(data['covers'].length > 0) {
              let wishlistString = ""
              let wishlist = []

              if(data['covers'][0]['cover']['model']['modelData']['formFieldsEntities']['wishlist']['products'])
                wishlistString = data['covers'][0]['cover']['model']['modelData']['formFieldsEntities']['wishlist']['products'];

              let userWishListId = data['covers'][0]['id'];
              
              wishlist = wishlistString.split(",")
              if(wishlist.includes(productId)) {
                let index = wishlist.findIndex(prodid => prodid === productId);
                wishlist.splice(index,1);
              }

              else {
                wishlist.push(productId)
              }

              wishlistString = wishlist.join(",")

              this.addToUserWishlist(productId, wishlistString,userWishListId )

            }

            else {

              let wishlist = productId+""
              this.addToUserWishlist(productId, wishlist,0 )
            }

          },
          error => {
    
          },

          () => {

          }
      );
    }
     //if not logged in create one in local.storage and prompt user to login

  }

  addToUserWishlist(productid, currentList, mid) {

    let modelObject1 = new FormData();

    modelObject1.append('modelName', 'WishList');
    modelObject1.append('id', mid);

    modelObject1.append('wishlist-products', currentList);


    this._data.updateModelData(modelObject1) 
      .pipe(first())
      .subscribe(
        data => {

          if(data['covers'][0]['cover']['model']['modelData']['formFieldsEntities']['wishlist']['products']) {
            let wishlist = data['covers'][0]['cover']['model']['modelData']['formFieldsEntities']['wishlist']['products'];
            localStorage.setItem('WishList', JSON.stringify(wishlist));
          }
        },
        error => {
        },

        () => {
  
        }
      );

  }

  cleanSessionOrderDetails() {
    sessionStorage.removeItem('order');
    localStorage.removeItem('cartItems');
    sessionStorage.removeItem('txn');

    this.cartItemsChanged.next([]);
  }

  addToViewedProducts() {
    //check if user is signed in, if yes add the product id to the users viewed products
  }

  getSellerOrders() {

  }

  getSellerAccountDetails() {

  }

  calculatetaxBracket(bracket, price) {

    let tax = 0

    switch (bracket) {

      case "No-Vat":

        tax = 0
        
        break;

      case "Standard-Rate-Purchases-15":

        tax = price * 0.15
      
        break;
    
      case "Standard-Rate-Purchases-Capital-Goods-15":

        tax = price * 0.15
        
        break;
    
      case "Old-Change-in-Use-14":

        tax = price * 0.14
        
        break;
    
      case "Zero-Rate-Exported-Goods":

        tax = 0
        
        break;
    
      case "Zero-Rated-Purchases":

        tax = 0
        
        break;
    
    
      default:
        break;
    }

    return tax

  }

  addToUserViewedProducts() {

  }

  getValidProducts(products) {
    // this.products.sort((a,b)=>a['cover']['model']['modelData']['formFieldsEntities']['productDetails']['name'].localeCompare(b['cover']['model']['modelData']['formFieldsEntities']['productDetails']['name']))
    // return this.products.sort((a,b)=>b['cover']['model']['modelData']['formFieldsEntities']['productDetails']['regular_price'] - a['cover']['model']['modelData']['formFieldsEntities']['productDetails']['regular_price'])

    return products.filter(prod => prod['cover']['model']['modelData']['formFieldsEntities']['ProductFlags']['published'] === false);
  }





















  //might not be used

  paymentRequest (data) {
    return this.http.post<any>(`${environment.apiUrl}/query-model-data`, data, httpOptions)
  } // make a payment request to the payment gateway service

  completeOrder (data) {
    return this.http.post<any>(`${environment.apiUrl}/update-model-data`, data, httpOptions)
  } // Get existing customer loan

  cancelOrder (data) {
    return this.http.post<any>(`${environment.apiUrl}/get-definition-object`, data, httpOptions)
  } // get the object nodel 

  refundOrder (data) {
    return this.http.post<any>(`${environment.apiUrl}/get-definition-object`, data, httpOptions)
  } // get the object nodel 

  getWishList(data) {
    return this.http.post<any>(`${environment.apiUrl}/get-definition-object`, data, httpOptions)
  } // get the object nodel 

  updateCart(data) {
    return this.http.post<any>(`${environment.apiUrl}/get-definition-object`, data, httpOptions)
  } // get the object nodel 

  // Get Requests - Common Requests
  
  getCart () {
    return this.http.get<any>(`${environment.apiUrl}/get-definition-object`, httpOptions)
  }
  
}
