import { TestBed } from '@angular/core/testing';

import { ExpressFunctionsService } from './express-functions.service';

describe('ExpressFunctionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExpressFunctionsService = TestBed.get(ExpressFunctionsService);
    expect(service).toBeTruthy();
  });
});
