import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainLayoutComponent } from './view-components/main-layout/main-layout.component';
import { BlankLayoutComponent } from './view-components/blank-layout/blank-layout.component';

import { ModelDetailsWithHistoryComponent } from './system-pages/application-pages/model-details-with-history/model-details-with-history.component';
import { DashboardLandingPageComponent } from './system-pages/application-pages/dashboard-landing-page/dashboard-landing-page.component';
import { DashboardOfEnabledmodulesComponent } from './system-pages/application-pages/dashboard-of-enabledmodules/dashboard-of-enabledmodules.component';
import { UserContextModelsComponent } from './system-pages/application-pages/user-context-models/user-context-models.component';
import { ModelListViewComponent } from './system-pages/application-pages/model-list-view/model-list-view.component';
import { LoginPageComponent } from './system-pages/auth-pages/login-page/login-page.component';
// import { AuthLayoutComponent } from './shared/components/layouts/auth-layout/auth-layout.component';
import { AuthGuard } from './_services/auth.guard';
import { AddModelComponent } from './system-pages/application-pages/add-model/add-model.component';

//Module Pages
import { ECommerceLandingComponent } from './system-pages/modules-pages/e-commerce/e-commerce-landing/e-commerce-landing.component';
import { CourierLandingPageComponent } from './system-pages/modules-pages/courier/courier-landing-page/courier-landing-page.component';
import { CatalogueDepartmentsComponent } from './system-pages/modules-pages/e-commerce/catalogue-departments/catalogue-departments.component';
import { CatalogueDashboardComponent } from './system-pages/modules-pages/e-commerce/catalogue-dashboard/catalogue-dashboard.component';
import { ViewSellerDashboardComponent } from './apps/e-commerce-app/view-seller-dashboard/view-seller-dashboard.component';

//Accounts Module
import { AccountsDashboardComponent } from './system-pages/modules-pages/accounts/accounts-dashboard/accounts-dashboard.component';
import { WalletCashoutRequestsComponent } from './system-pages/modules-pages/accounts/wallet-cashout-requests/wallet-cashout-requests.component';
import { WalletSettleCashoutsComponent } from './system-pages/modules-pages/accounts/wallet-settle-cashouts/wallet-settle-cashouts.component';
import { AccountsBalanceSummaryComponent } from './system-pages/modules-pages/accounts/accounts-balance-summary/accounts-balance-summary.component';



//invoice app
import { InvoiceCreteNewComponent } from './invoice-app/invoice-crete-new/invoice-crete-new.component';



const routes: Routes = [

  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },

  //auth pages
  { path: 'sessions', component: LoginPageComponent},
  { path: 'sessions/login', component: LoginPageComponent },
  { path: 'sessions/reset-password', component: LoginPageComponent },



  //dashboard pages
  { path: 'dashboard-modules', component: DashboardOfEnabledmodulesComponent, canActivate: [AuthGuard]},
  { path: 'dashboard', component: DashboardOfEnabledmodulesComponent, canActivate: [AuthGuard]},
  { path: 'dashboard/home', component: DashboardOfEnabledmodulesComponent, canActivate: [AuthGuard] },
  // { path: 'dashboard', component: DashboardLandingPageComponent, canActivate: [AuthGuard]},
  // { path: 'dashboard/home', component: DashboardLandingPageComponent, canActivate: [AuthGuard] },

  { path: 'dashboard/model-details', component: ModelDetailsWithHistoryComponent, canActivate: [AuthGuard] }, //to be removd
  { path: 'dashboard/model-details/:modelName/:id', component: ModelDetailsWithHistoryComponent, canActivate: [AuthGuard], runGuardsAndResolvers: 'always' }, 
  { path: 'dashboard/model-details-view/:modelName/:id', component: ModelDetailsWithHistoryComponent, canActivate: [AuthGuard], runGuardsAndResolvers: 'always' }, 

  { path: 'dashboard/model-history', component: ModelDetailsWithHistoryComponent, canActivate: [AuthGuard] },

  { path: 'dashboard/context-models', component: UserContextModelsComponent, canActivate: [AuthGuard] }, //to be removd
  { path: 'dashboard/context-models/:modelName/:contextId', component: UserContextModelsComponent, canActivate: [AuthGuard] },

  { path: 'dashboard/model-list/:modelName', component: ModelListViewComponent, canActivate: [AuthGuard] }, //to be removd
  
  { path: 'dashboard/add-model/:modelName', component: AddModelComponent, canActivate: [AuthGuard] }, //to be removd
  
  // { path: 'dashboard/invoice/new', component: InvoiceDetailComponent, canActivate: [AuthGuard] }, 
  // { path: 'dashboard/invoice/edit/:id', component: InvoiceDetailComponent, canActivate: [AuthGuard] }, 
  // { path: 'dashboard/invoice/list', component: InvoiceListComponent, canActivate: [AuthGuard] }, 
  { path: 'dashboard/invoice/create/:id', component: InvoiceCreteNewComponent, canActivate: [AuthGuard] }, 

  //E-commerce modules
  { path: 'dashboard/ecommerce-modules/dashboard', component: ECommerceLandingComponent, canActivate: [AuthGuard] },
  { path: 'dashboard/ecommerce-modules/departments', component: CatalogueDepartmentsComponent, canActivate: [AuthGuard] },
  { path: 'dashboard/ecommerce-modules/catalogue-dashboard', component: CatalogueDashboardComponent, canActivate: [AuthGuard] },
  { path: 'dashboard/courier-modules/dashboard', component: CourierLandingPageComponent, canActivate: [AuthGuard] },

  //sellers
  { path: 'dashboard/ecommerce-modules/seller/:id', component: ViewSellerDashboardComponent, canActivate: [AuthGuard] },

  //accounts module
  { path: 'dashboard/accounts/dashboard', component: AccountsDashboardComponent, canActivate: [AuthGuard] },
  { path: 'dashboard/accounts/balances', component: AccountsBalanceSummaryComponent, canActivate: [AuthGuard] },
  


  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
